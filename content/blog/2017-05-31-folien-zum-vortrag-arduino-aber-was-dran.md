+++
title = "Folien zum Vortrag: Arduino - aber was dran?"
date = 2017-05-31T09:29:19Z
author = "typ_o"
path = "/blog/2017/05/31/folien-zum-vortrag-arduino-aber-was-dran"
aliases = ["/blog/archives/379-Folien-zum-Vortrag-Arduino-aber-was-dran.html"]
+++
Hier noch von unserer letzten Veranstaltung "Was Sie schon immer über
Technik wissen wollten..." die Vortragsfolien zu "Arduino - aber was
dran?".

[FD-VortragArduinoaberwasdran.zip](/media/FD-VortragArduinoaberwasdran.zip "FD-VortragArduinoaberwasdran.zip")

Das ist eine umangreiche Übersicht mit vielen Praxistips zum Thema
Mikrocontroller - Peripherie, natürlich nicht auf Arduino beschränkt.
Von [5 Volt Junkie](https://5volt-junkie.net/). Super Talk, kthx!  

[![](/media/dcmotor.serendipityThumb.png)](/media/dcmotor.png)

Eine der 30 Folien
