+++
title = "Was braucht man als erstes?"
date = 2010-01-18T19:54:00Z
author = "typ_o"
path = "/blog/2010/01/18/was-braucht-man-als-erstes"
aliases = ["/blog/archives/64-Was-braucht-man-als-erstes.html"]
+++
Schwarze T - Shirts und Kaputzenshirts!

[![](/media/sw_mit_logo.serendipityThumb.jpg)](/media/sw_mit_logo.jpg)

Preview

[![](/media/fuer-reinen-gelbdruck_520.serendipityThumb.jpg)](/media/fuer-reinen-gelbdruck_520.jpg)

Detail

Habe eben Spreadshirt und einen Anbieter aus Kassel verglichen, sehr
ähnlich, und vor Ort kann man das Shirt auch mal anfassen. ([CorelDraw
Datei in
Pfaden](/media/fuerreinengelbdruck.cdr "fuerreinengelbdruck.cdr")).

Edit: Ganz auf die Schnelle kann man sofort [hier im Shop
bestellen](https://flipdot.spreadshirt.de/).
