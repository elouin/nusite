+++
title = "Heute back' ich, morgen brau' ich, und übermorgen ..."
date = 2016-08-11T16:33:36Z
author = "typ_o"
path = "/blog/2016/08/11/heute-back-ich-morgen-brau-ich-und-ubermorgen"
aliases = ["/blog/archives/353-Heute-back-ich,-morgen-brau-ich,-und-uebermorgen-....html"]
+++
Pizzaworkshop war schon - jetzt gibt's eine Einführung in das Brauen von
unserem Neu-Member and….

![](/media/br.serendipityThumb.jpg)

Am 14.08.16 wird um 12:00 Uhr eingemaischt. Wenn du noch nicht auf der
Teilnehmerliste stehst, maile an com /kringeldings/ flipdot punkt org  
Bis zur fertigen Würze, also der Punkt wo das Handwerk aufhört und die
Hefe sich daran macht die Bierherstellung zu übernehmen, werden etwa 6
Stunden benötigt.  
Anhand eines süffigen Pale-Ale, werden die einzelnen Schritte des
Brauens durchgegangen und die jeweiligen „Stellschrauben“ gezeigt, die
das ganze so interessant machen.

Edit: Der letzte Schritt, das zusetzen der Hefe, konnte leider nicht bei
uns im Space gemacht werden, sondern musste aus rechtlichen Gründen bei
unserem Fachmann zu Hause vorgenommen werden. Schade!

![](/media/br2.serendipityThumb.jpg)
