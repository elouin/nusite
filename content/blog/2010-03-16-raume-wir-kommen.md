+++
title = "Räume, wir kommen!"
date = 2010-03-16T09:11:36Z
author = "typ_o"
path = "/blog/2010/03/16/raume-wir-kommen"
aliases = ["/blog/archives/77-Raeume,-wir-kommen!.html"]
+++
Heute wieder 19:00 im
[Kunsttempel](https://flipdot.org/blog/archives/47-Ab-jetzt-immer-Dienstags.html).
Topics der öffentlichen Mitgliederversammlung (Gäste, Interessenten usf.
willkommen!) sind unter anderem eine abschließende Entscheidung über
unsere Räumlichkeiten, kurz was über unsere Beitragsordnung und die
Frage, welche Versicherung brauchen wir.
