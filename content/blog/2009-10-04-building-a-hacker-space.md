+++
title = "Building a Hacker Space"
date = 2009-10-04T18:29:20Z
author = "typ_o"
path = "/blog/2009/10/04/building-a-hacker-space"
aliases = ["/blog/archives/19-Building-a-Hacker-Space.html"]
+++
[![](/media/ohlig.jpg)](https://chaosradio.ccc.de/24c3_m4v_2133.html)

Wie hier schon mit einem Link auf das Wiki von hackerspaces.org
[erwähnt](https://flipdot.org/blog/archives/2-Erst-Ei,-dann-Gack!.html),
heute noch das [Video zum Vortrag von Lars Weiler und Jens
Ohlig](https://chaosradio.ccc.de/24c3_m4v_2133.html) zum Hacker Space
Design Pattern Catalogue auf dem
[24C3](https://events.ccc.de/congress/2007/Fahrplan/events/2133.en.html).
Etwas krudes CCC-Englisch, aber hübsche Folien, letztere
[hier](https://events.ccc.de/congress/2007/Fahrplan/attachments/1003_Building%20a%20Hacker%20Space.pdf)
als pdf.
