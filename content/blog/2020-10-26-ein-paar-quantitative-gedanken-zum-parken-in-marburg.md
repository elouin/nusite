+++
title = "Ein paar (quantitative) Gedanken zum Parken in Marburg"
date = 2020-10-26T12:56:02Z
author = "Baustel"
path = "/blog/2020/10/26/ein-paar-quantitative-gedanken-zum-parken-in-marburg"
aliases = ["/blog/archives/448-Ein-paar-quantitative-Gedanken-zum-Parken-in-Marburg.html"]
+++
Was macht euer Auto in seiner Freizeit? Es parkt herum.

Das hat sich Martin in diesem kleinen Hobby-Projekt für den Fall in
Marburg mal etwas genau angesehen. Warnung: Mit etwas genenauer meine
ich hier eigentlich viel zu genau.

![](/media/vortrag-parken-marburg.serendipityThumb.png)

Um nicht zu viel zu verraten bleibt es bei dieser mysteriösen
Beschreibung - kommt also lieber selber vorbei und schaut euch das etwa
20 minütige Spektakel an.

Der Vortrag findet am Dienstag, den 27.10. statt. Wir quatschen ab etwa
19 Uhr im Mumble (mumble.flipdot.org) und der Vortrag beginnt um 20 Uhr
im Jitsi (<https://meet.ffmuc.net/flipdot>).
