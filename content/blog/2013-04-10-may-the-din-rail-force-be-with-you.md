+++
title = "May the (DIN rail) Force be with you"
date = 2013-04-10T15:17:45Z
author = "typ_o"
path = "/blog/2013/04/10/may-the-din-rail-force-be-with-you"
aliases = ["/blog/archives/192-May-the-DIN-rail-Force-be-with-you.html"]
+++
![](/media/force.jpg)

Recycling: Aufmachen, Sand (Sand? Sand!) rausschütteln, Spannung von 24
V auf 5 V und 12 V umbauen, zumachen.
