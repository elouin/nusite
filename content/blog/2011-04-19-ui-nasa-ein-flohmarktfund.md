+++
title = "Ui! NASA! Ein Flohmarktfund"
date = 2011-04-19T18:10:22Z
author = "typ_o"
path = "/blog/2011/04/19/ui-nasa-ein-flohmarktfund"
aliases = ["/blog/archives/136-Ui!-NASA!-Ein-Flohmarktfund.html"]
+++
![](/media/demo.jpg)  
Samstags auf meinem Lieblingsflohmarkt einen Seefunkempfänger ergattert
(7 EUR). Mangels See und Schiff, bzw. aufgrund zu großen Abstandes zu
den auf der Mittelwelle arbeitenden Fernschreibstationen, die für diesen
Empfänger die Wettertexte senden, habe ich ihn gleich mal zerlegt, die
passenden Datenblätter zu den Bauteilen drin herausgesucht und ein paar
Zeilen Programm zur Ansteuerung des LCD geschrieben - for further use.
