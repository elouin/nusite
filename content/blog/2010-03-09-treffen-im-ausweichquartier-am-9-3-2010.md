+++
title = "Treffen im Ausweichquartier am 9.3.2010"
date = 2010-03-09T15:15:00Z
author = "typ_o"
path = "/blog/2010/03/09/treffen-im-ausweichquartier-am-9-3-2010"
aliases = ["/blog/archives/74-Treffen-im-Ausweichquartier-am-9.3.2010.html"]
+++
Heute kann unser Treffen wegen einer Ausstellung nicht im Kunsttempel
stattfinden, deswegen Ausweichquartier in der Pizzeria Da Toni,
www.pizza-datoni.de, Friedrich-Ebert-Straße 26, 34117 Mitte, Kassel 0561
12001. Wir sind ab **19:00 Uhr** im Obergeschoß!
