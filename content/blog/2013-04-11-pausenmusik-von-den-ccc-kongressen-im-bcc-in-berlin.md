+++
title = "Pausenmusik von den CCC Kongressen im BCC in Berlin"
date = 2013-04-11T08:42:35Z
author = "typ_o"
path = "/blog/2013/04/11/pausenmusik-von-den-ccc-kongressen-im-bcc-in-berlin"
aliases = ["/blog/archives/193-Pausenmusik-von-den-CCC-Kongressen-im-BCC-in-Berlin.html"]
+++
![](/media/pause1.jpg)

[Hier](https://labs.echonest.com/Uploader/index.html?trid=TRIGZGH13DF800F8BD)
als Endlosschleife mit zufälligen Sprüngen, nette Empfehlung von
[NSFW066](https://not-safe-for-work.de/nsfw066/).
