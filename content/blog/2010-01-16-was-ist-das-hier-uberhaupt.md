+++
title = "Was ist das hier überhaupt?"
date = 2010-01-16T11:33:44Z
author = "typ_o"
path = "/blog/2010/01/16/was-ist-das-hier-uberhaupt"
aliases = ["/blog/archives/62-Was-ist-das-hier-ueberhaupt.html"]
+++
[![](/media/vlcsnap-2010-01-16-08h44m06s122.serendipityThumb.jpg)](/media/vlcsnap-2010-01-16-08h44m06s122.jpg)

Spaß am Gerät

Der
[Plan](https://flipdot.org/blog/archives/5-Einladung-zum-Starttreffen-am-6.10.09.html),
auch in Kassel einen
[Hackerspace](https://de.wikipedia.org/wiki/Hackerspace) aufzubauen,
begann mit einem initialen Treffen im Oktober letzten Jahres, zu dem 28
Teilnehmer mit so vielen Zielen und Ideen kamen, daß viele sich fragten:
"Warum erst jetzt?".  
In der Zwischenzeit beteiligte sich die Gruppe mit einem offenen
[Workshop](https://www.interfiction.org/abstracts-cv/helmut-fligge/) auf
der [Interfiction](https://www.interfiction.org/) (hier ein
[Video](https://flipdot.org/blog/archives/53-flipdot-trifft-Medientheoretiker-und-macht-Praxis.html)
dazu), der Hackerspace "SubLab" in Leipzig wurde
[besucht](https://flipdot.org/blog/archives/45-Besuch-im-SubLab-in-Leipzig.html),
schnell gründete man einen gemeinnützigen Verein und inzwischen steht
auch die Raumsuche vor dem Abschluß.  

[![](/media/vlcsnap-2010-01-16-08h45m28s160.serendipityThumb.jpg)](/media/vlcsnap-2010-01-16-08h45m28s160.jpg)

Hardware Hacking

flipdot besteht zur Zeit aus 16 Menschen, die ab Januar monatlich 36EUR
dafür aufbringen, sich einen gemeinsamen Raum zu schaffen, in dem die
Technik auch mal gegen den Strich gebürstet werden kann. Darunter sind
Programmierer, Künstler, Hardware-Spezialisten aus verschiedenen
Bereichen, Auszubildende und Studenten verschiedener meist technischer
Richtungen. Ein Teil der Initiative ist gleichzeitig Mitglied im lokalen
[Erfa-Kreis](https://www.ccc.de/de/club/erfas) des [Chaos Computer
Club](https://ccc.de/).  

[![](/media/vlcsnap-2010-01-16-08h44m55s115.serendipityThumb.jpg)](/media/vlcsnap-2010-01-16-08h44m55s115.jpg)

Blinkende Dinge

Derzeit ist einer der Favoriten der Raumsuche das ehemalige Zollamt am
Hauptbahnhof, die Gruppe trifft sich morgen um 13:50 vor dem Burger King
im Bahnhofsfoyer, um das Gebäude gemeinsam zu besichtigen, dabei sind
Interessierte willkommen.

Bis ein Raum gefunden ist, wird es weitere gemeinsame und offene
Aktionen wie den Workshop auf der Interfiction geben, Ankündigungen dazu
im [Blog](/blog/) der Initiative. Die wöchentlichen
Treffen von "flipdot - hackerspace kassel" sind jeden [Dienstag um 19:00
im
Kunsttempel](https://flipdot.org/blog/archives/47-Ab-jetzt-immer-Dienstags.html).
