+++
title = "Mumble"
+++

Mit Mumble kannst du mit Personen im flipdot online reden. Dazu brauchst du nur einen Mumble Client:

* [Offizieller Mumble Client](https://www.mumble.info/downloads/)
* Mumla für Android (Auf [F-Droid](https://f-droid.org/packages/se.lublin.mumla) oder Google Play)

Und kannst dich mit diesem zu unserem Server verbinden:

```
Adresse: mumble.flipdot.org
   Port: 64738
```

## RNNoise

Mit Hilfe von [RNNoise](https://jmvalin.ca/demo/rnnoise/) kann man Hintergrundgeräusche deutlich reduzieren. Das hilft Anderen dabei dich besser zu verstehen. Du kannst es wie folgt einschalten:

```
Configure > Settings... > Audio Input > Audio Processing > RNNoise
```
