+++
title = "\"Dauerinstallation\""
date = 2013-04-21T13:25:13Z
author = "typ_o"
path = "/blog/2013/04/21/dauerinstallation"
aliases = ["/blog/archives/201-Dauerinstallation.html"]
+++
![](/media/raspi-dauer.jpg)

Der Raspberry aktualisiert den Space-Status im Blog und auf Twitter
(demnächst auch per mail) und schaltet per 433 MHz - ISM - Funk
Steckdosen im Space ein, sobald aufgeschlossen wurde (und beim Verlassen
automatisch wieder ab)
