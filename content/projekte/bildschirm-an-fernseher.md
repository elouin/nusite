+++
title = "Bildschirm an Fernseher"
+++

Um den eigenen Bildschirminhalt an den Fernseher zu streamen, einfach
mit einem vnc server zum pi-display verbinden. z.B.

    x11vnc -connect pi-display
