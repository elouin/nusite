+++
title = "IoT-Hardware: Change Requests"
+++

Aktuell PCB V0.4

- Footprint von C1 von Elko auf Kerko ändern
- Footprint von C2 - C4 von Kunnstofffolie auf Kerko ändern
- Iso-Spalt bei den Lötjumpern wesentlich kleiner
- Dünne Tracks bei den Lötjumpern für default Brücken legen
- J3 durch Taster ersetzen?
- Funktionsvariante "Attiny nicht bestückt, Sensortaster über Mosfet, ESP steuert Mosfet zur
  Selbsthaltung" vorsehen. Das geht aktuell mit J1 und J5 schon wenn der Attiny nicht bestückt ist,
  es fehlt nur Taster-Anschluss über dem Mosfet.
- Eine WS2812 LED aufs Board für chique Signalisierung?
