+++
title = "Die Gentrifizierung der Hacker und Maker Szene"
date = 2016-02-14T04:47:31Z
author = "typ_o"
path = "/blog/2016/02/14/die-gentrifizierung-der-hacker-und-maker-szene"
aliases = ["/blog/archives/331-Die-Gentrifizierung-der-Hacker-und-Maker-Szene.html"]
+++
Der Hessische Rundfunk - eine der millionenschweren
öffentlich-rechtlichen Landesrundfunkanstalten in Deutschland, macht
diese Wochenende einen
[Hackaton](https://hessenschau.de/gesellschaft/1-hr-hackathon,hackathon-102.html).
In den Infos dazu heißt es: "Es geht nicht darum, dass der hr sich
billig gute Projekte liefern lassen will".

Der Kongress des CCC [hatte 2015 über 10.000
Teilnehmer](https://de.wikipedia.org/wiki/Chaos_Communication_Congress#Kongress-Mottos_und_Veranstaltungsorte_1984_bis_heute),
die Hackerszene franst an den Rändern ganz enorm aus in die Makerspaces,
in die Fablabs und auch in Startup-Inkubatoren wie das
[Betahaus](https://www.betahaus.com/berlin/).

Wieso nur hat diese Kommerzialisierung einen so unappetitlichen
Beigeschmack?

Die umgedeutete Definition des StartUp Unternehmers im
Technologiebereich zu einem "Hacker" ist Teil eines entstehenden Systems
eines Silicon-Valley
[Doppeldenks](https://de.wikipedia.org/wiki/Doppeldenk), schreibt Brett
Scott in seinem Text "[The hacker
hacked](https://aeon.co/essays/how-yuppies-hacked-the-original-hacker-ethos)
- The hacker ethos is wild and anarchic, indifferent to the trappings of
success. Or it was, until the gentrifiers moved in".

Eine Parallelität sehe ich z.B. in der Entwicklung von
gratis-Übernachtungsbörsen zu Game-Changern im Übernachtungsgewerbe wie
[AirBnb](https://www.zeit.de/2012/34/Airbnb-Wohnungsvermietung-Nathan-Blecharczyk/komplettansicht).
"Sie werden assimiliert, Widerstand ist zwecklos" ist das implizite
Motto des Kapitalismus. AirBnb [treibt beispielsweise in Berlin die
Gentrifizierung
voran](https://www.faz.net/aktuell/wirtschaft/immobilien/im-wrangelkiez-zeigt-sich-wie-airbnb-kreuzberg-verhoekert-13319651.html).

Uber frißt das Taxigewerbe, und was im Internet ursprünglich mit einer
Ausschaltung der profitierenden Mittelsmänner begann, endet in einer
[Auflösung von geschützten und geregelten
Arbeitsverhältnissen](https://www.spiegel.de/spiegel/vorab/verkehrsminister-uber-bedroht-den-rechtsstaat-a-991453.html)
im großen Stil.

Vorerst empfinde ich nur Beunruhigung und Irritation - ich habe Sorge,
dass der "Hacker" sich vereinnahmen läßt und hilft, das Internet (und
unser System) weiter umzubauen - von einem Ort der ein
gesellschaftlicher Gegenentwurf war (wie naiv auch immer), zu einem
Marktplatz gieriger Turbokapitalisten und einer bizarren
protofaschistischen Überwachungsmaschine.

Es liegt an uns. Ist es unser System? **Hacking, then, looks like a
practice with very deep roots – as primally and originally human as
disobedience itself. Which makes it all the more disturbing that hacking
itself appears to have been hacked.**
([Scott](https://aeon.co/essays/how-yuppies-hacked-the-original-hacker-ethos))

![](/media/18c3-logo-tastatur-stern.jpg)
