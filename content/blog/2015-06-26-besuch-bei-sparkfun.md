+++
title = "Besuch bei Sparkfun"
date = 2015-06-26T18:03:02Z
author = "flipdot member"
path = "/blog/2015/06/26/besuch-bei-sparkfun"
aliases = ["/blog/archives/307-Besuch-bei-Sparkfun.html"]
+++
Im Zuge des AutonomousVehicleContest (AVC) gab es eine sehr spannende  
Führung durch das nagelneue Firmengebäude.

[![Besuch bei
Sparkfun](/media/IMG_20150620_142720.serendipityThumb.jpg)](/media/IMG_20150620_142720.jpg)
