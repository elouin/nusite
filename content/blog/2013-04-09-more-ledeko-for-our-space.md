+++
title = "More LEDeko for our space"
date = 2013-04-09T15:43:39Z
author = "typ_o"
path = "/blog/2013/04/09/more-ledeko-for-our-space"
aliases = ["/blog/archives/191-More-LEDeko-for-our-space.html"]
+++
An old Roentgen film viewer combied with a handful of [WS2801 IC with
three RGB LED
each](https://shop.led-studien.de/de/elektronik-bausatze/led-pixel).
Looks like a pretty high resolution diplay although only 8 RGB channels
are used.

Die Idee kam mir auf dem Easterhegg in Paderborn, da gabs hinter einer
Wandverkleidung mit tausend Löchern ein paar LED, die prima
rüberkamen...

Die Blende ist aus Pertinax (ahh, der Geruch!) und auf der CNC-Fräse
geschnitten. (Allerdings noch nicht auf der aus dem Space, die wird erst
kommenden Samstag konstruiert)
