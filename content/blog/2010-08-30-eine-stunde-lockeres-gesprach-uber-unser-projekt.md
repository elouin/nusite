+++
title = "Eine Stunde lockeres Gespräch über unser Projekt"
date = 2010-08-30T11:13:51Z
author = "typ_o"
path = "/blog/2010/08/30/eine-stunde-lockeres-gesprach-uber-unser-projekt"
aliases = ["/blog/archives/105-Eine-Stunde-lockeres-Gespraech-ueber-unser-Projekt.html"]
+++
Hier der Mitschnitt von der Sendung im Freien Radio am gestrigen
Sonntag. Wir haben uns eine Stunde locker mit Salm und Reuter
unterhalten. Technische Tiefe angepaßt an die Radiohörer - also bei
weitem nicht CRE-Level :)  
Worum gings: Was ist ein Hackerspace, was machen wir da, wie viele sind
wir, wo sind die Frauen, wir bauen uns ein Paradies (Salm), 3D-Drucken,
Projekte, Finanzierung.

Download:
[2010-08-29\_FRK.mp3](/media/2010-08-29_FRK.mp3 "2010-08-29_FRK.mp3")
