+++
title = "Neulich am Open-Space-Dienstag"
date = 2018-08-08T21:16:38Z
author = "typ_o"
path = "/blog/2018/08/08/neulich-am-open-space-dienstag"
aliases = ["/blog/archives/411-Neulich-am-Open-Space-Dienstag.html"]
+++
Schnurrgenerator für synthetische Katze zum um-den-Hals-hängen:

[![](/media/20180710_195432.serendipityThumb.jpg)](/media/20180710_195432.jpg)

Neues Netzteil:

[![](/media/20180710_195112.serendipityThumb.jpg)](/media/20180710_195112.jpg)

Audio-Synthese im Browser:

[![](/media/20180710_194740.serendipityThumb.jpg)](/media/20180710_194740.jpg)

Visualisierung der synthetisierten Signale auf der
Analogmultiplizierer-Mimik:

[![](/media/20180710_194711.serendipityThumb.jpg)](/media/20180710_194711.jpg)

Schalterreparatur an der Ständerbohrmaschine:

[![](/media/20180710_195028.serendipityThumb.jpg)](/media/20180710_195028.jpg)

PoE versorgte Schaltung zum Schliessen der Dachluke:

[![](/media/20180710_194913.serendipityThumb.jpg)](/media/20180710_194913.jpg)

WS2812 enhancement für eine Blinkleuchte:

[![](/media/20180710_194650.serendipityThumb.jpg)](/media/20180710_194650.jpg)
