+++
title = "Blender-Crashkurs"
+++

Hier ein minimales Tutorial, was dir erklärt, wie man sich innerhalb von
Blender 2.6+ bewegt.

## Ansicht ändern

-   \_\_
        Mittlere Maustaste (MMB)

    \_\_ gedrückt halten, um die Kamera um das zentrierte Objekt zu
    drehen
-   \_\_
        Shift + MMB

    \_\_ gedrückt halten für Panning (seitliches Verschieben)
-   \_\_
        Entf (Ziffernblock)

    \_\_ drücken, um die Kamera auf das ausgewählte Objekt zu zentrieren

    -   -   Vorher \_\_
                Rechte Maustaste (RMB)

            \_\_ klicken, um ein Objekt anzuwählen
-   Tasten auf dem Ziffernblock drücken, um die Ansicht zu ändern:
    -   -   \_\_
                1

            \_\_ Frontalansicht
        -   \_\_
                3

            \_\_ Rechtsaufsicht
        -   \_\_
                7

            \_\_ Vogelperspektive
        -   \_\_
                8

            \_\_ Hoch
        -   \_\_
                2

            \_\_ Runter
        -   \_\_
                4

            \_\_ Links
        -   \_\_
                6

            \_\_ Rechts
        -   \_\_
                5

            \_\_ Wechseln zwischen Perspektive (an/aus)

## Objekte ändern

-   \_\_
        Rechte Maustaste (RMB)

    \_\_ klicken, um ein Objekt anzuwählen
-   \_\_
        G

    \_\_ drücken, um in den *Bewegungsmodus* zu kommen.

    -   -   In diesem Modus \_\_
                X

            \_\_, \_\_

                Y

            \_\_ oder \_\_

                Z

            \_\_ drücken, um auf einer der Achsen einzurasten
        -   \_\_
                Shift + X

            \_\_, \_\_

                Shift + Y

            \_\_ und \_\_

                Shift + Z

            \_\_ ausprobieren
        -   \_\_
                Shift

            \_\_ gedrückt halten, während man die Maus bewegt, sorgt für
            genauere Bewegungen
        -   \_\_
                Strg

            \_\_ gedrückt halten, während man die Maus bewegt, sorgt
            fürs Bewegen im Raster

`    * __`

    Shift + Strg

\_\_ für ein genaueres Raster

-   \_\_
        R

    \_\_ drücken, um in den *Rotationsmodus* zu gelangen
-   \_\_
        S

    \_\_ drücken, um in den *Skalierungsmodus* zu gelangen
