+++
title = "35. CCC Congress auch in Kassel, vom 27. - 30. Dez."
date = 2018-12-16T13:23:53Z
author = "typ_o"
path = "/blog/2018/12/16/35-ccc-congress-auch-in-kassel-vom-27-30-dez"
aliases = ["/blog/archives/423-35.-CCC-Congress-auch-in-Kassel,-vom-27.-30.-Dez..html"]
+++
Auch dieses Jahr findet wieder der [Chaos Communication
Congress](https://de.wikipedia.org/wiki/Chaos_Communication_Congress)
statt, ausgerichtet vom [Chaos Computer
Club](https://de.wikipedia.org/wiki/Chaos_Computer_Club). Erwartet
werden 15.000 Besucher. Für alle die nicht nach Leipzig fahren können:
Wir schauen bei uns im [Hackerspace](/kontakt/)
ausgewählte Vorträge aus dem
[Programm](https://fahrplan.events.ccc.de/congress/2018/Fahrplan/) live
an, unterhalten uns darüber, bausteln an unseren Projekten weiter und
kochen was leckeres.

So ca. ab Mittag sind unsere Türen geöffnet. Ihr seht rechts oben auf
dieser Seite sobald geöffnet ist. Seid willkommen! Eintritt frei.

[![](/media/33c3.serendipityThumb.jpg)](/media/33c3.jpg)
