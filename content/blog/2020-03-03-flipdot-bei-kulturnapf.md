+++
title = "flipdot bei Kulturnapf"
date = 2020-03-03T22:01:22Z
author = "Baustel"
path = "/blog/2020/03/03/flipdot-bei-kulturnapf"
aliases = ["/blog/archives/438-flipdot-bei-Kulturnapf.html"]
+++
Wir haben beim Kulturnapf Kassel ein Interview über uns gegeben. Erfahrt
mehr über uns im Podcast-Format!

<a href="#" onclick="flipdot.loadIframe(event, 'iframe_soundcloud_kulturnapf', 'https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/770085973&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true')"><img src="/media/soundcloud_kulturnapf_cookies.png" width="675" height="166" /></a>
<iframe class="hidden" scrolling="no" id="iframe_soundcloud_kulturnapf"></iframe>

[Interview auf
Soundcloud](https://soundcloud.com/user-976695732-112743983/s2e1kulturnapf-interview-flipdot)

Hört euch auch die anderen Folgen an: [Kulturnapf_Kassel auf
Soundcloud](https://soundcloud.com/user-976695732-112743983)
