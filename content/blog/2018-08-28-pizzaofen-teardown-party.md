+++
title = "Pizzaofen-Teardown-Party!"
date = 2018-08-28T11:49:55Z
author = "typ_o"
path = "/blog/2018/08/28/pizzaofen-teardown-party"
aliases = ["/blog/archives/414-Pizzaofen-Teardown-Party!.html"]
+++
Am 1.9.2018 ab 19:00 - unser Ofen muss raus aus der Küche, deswegen wird
nochmal ordentlich gebacken. Teig, Käse und Tomaten sind da, bring
deinen eigenen Belag mit!
([Anfahrt](/kontakt/))

[![](/media/pizza.serendipityThumb.JPG)](/media/pizza.JPG)
