+++
title = "Beitragsordnung"
+++

Die Beitragsordnung wurde am 19.6.2012 von der Mitgliedersversammlung wie folgt
beschlossen:

§ 01 Höhe der Beiträge

1. Der monatliche Beitrag liegt für natürliche Personen bei 23 Euro.
1. Für juristische Personen liegt er bei monatlich 54 Euro.
1. Fördermitglieder zahlen einen Beitrag von monatlich mindestens 54 Euro, der
   von ihnen mit der Beitrittserklärung festzulegen ist.
1. In besonderen Fällen kann der Vorstand für ein Mitglied eine Reduzierung des
   Beitrags beschließen. 

§ 02 Ermäßigung

1. Der ermäßigte Beitrag beträgt für natürliche Personen monatlich 10 Euro.
1. Um die Ermäßigung zu erhalten, ist eine begründete Mitteilung in Textform an
   den Vorstand erforderlich. 

§ 03 Fälligkeit/Zahlungweise

1. Der Beitrag wird am 1. Tag des Kalendermonats fällig.
1. Er wird unabhängig vom Beitrittstag für den Monat fällig, in dem der Beitritt
   erfolgt ist. In diesem Fall ist der Beitrag am Eintrittstag fällig.
1. Die Zahlung des Beitrages erfolgt im Lastschriftverfahren oder per
   Überweisung.
