+++
title = "Unser CAM Workflow"
date = 2015-06-26T17:04:04Z
author = "flipdot member"
path = "/blog/2015/06/26/unser-cam-workflow"
aliases = ["/blog/archives/306-Unser-CAM-Workflow.html"]
+++
Das Fräsen-Setup ist jetzt erstmal stabil, mit viel Platz für
Verbesserungen

\#1 CAD - Zeichnen  
z.B. Librecad, Rhino oder CorelDraw, dann Export als dxf

\#2 CAM - Fräsbahnberechnung und GCODE-Erzeugung  
Rhinocam oder ESTLCAM, schreiben GCODE

\#3 GCODE-Versand  
Universal Gcode Downloader (Java)

\#4 GCODE nach Schrittimpulse  
GRBL 0.9g auf einem Arduino

\#5 Motoransteuerung  
Triple Beast von Sorotec
