+++
title = "DMX"
+++

Ein Ethernet nach DMX Konverter mit AVR DMX-Empfängern

Lichtsteuerung (IP DMX: 192.168.3.115 TCP)

- [Steuerung Netzintern per OLA-API - easy DMX
  programming](http://opendmx.net/index.php/OLA_Python_API)
- [Steuerung Netzintern per OLA Client API - the C++
  API](https://docs.openlighting.org/ola/doc/latest/client_tutorial.html)
- [Steuerung Netzintern per
  WebUI](http://192.168.3.115:9090)

Materialsammlung

- [Arduino DMX
  Library](http://playground.arduino.cc/Learning/DMX)
- Arduino DMX Shield, z.B.
  [dies](https://www.tindie.com/products/Conceptinetics/dmx-shield-for-arduino-rdm-capable/)
- [ArtNet](http://art-net.org.uk/) (DMX over
  [UDP](http://en.wikipedia.org/wiki/User_Datagram_Protocol))

Kosten

- ArtNet Konverter (Ethernet -\> RS485)
- AVR DMX-Empfänger
  - Kaufen
    ([Arduino](http://www.dx.com/de/p/funduino-uno-r3-atmega328-development-board-for-arduino-384207) +
    [Shield](http://www.dx.com/de/s/arduino+dmx)): 18 €
  - Bauen 9 € (XLR Buchsen weglassen, da kein Bühneneinsatz)
- RS485 Kabel: 0 €, 200 m vorhanden (typ_o)
- [DMX-Kabel](http://www.thomann.de/de/the_sssnake_dmxkabel_3003.htm)
  3 pol. XLR (ca. 18x, 6,50 € = 117 € bei Thomann)
- Halogen Blinkenlight auf dem Kabelkanal (Geht mit der Arduino Lib
  und n MOSFETs)
- LED DMX
  [Leuchtleiste](http://www.thomann.de/de/stairville_led_bar_120_4_rgb_dmx_30_05m.htm?ref=search_rslt_dmx+led_355287_1)
  (8x, ca. 60,00 € = 480 € bei Thomann)
- WS2811 / -12 LED Ketten - gibts da eine Lib dazu?
- Strahler
- [Gobos](https://de.wikipedia.org/wiki/Gobo) - Dazu wäre
  gut der Glasdiaprojektor geeignet (typ_o)
- [Scanner](https://de.wikipedia.org/wiki/Scanner_(Lichttechnik))
  o.g. Projektor hat einen Spiegel, den man motorisch bewegen könnte

Web Sourcen

- DMX im
  [Backspace](https://www.hackerspace-bamberg.de/DMX_Lighting)
