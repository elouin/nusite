+++
title = "Hackers on a Train"
date = 2009-09-28T11:40:00Z
author = "typ_o"
path = "/blog/2009/09/28/hackers-on-a-train"
aliases = ["/blog/archives/8-Hackers-on-a-Train.html"]
+++
Ich würde gerne in der Aufbauphase einige Hackerspaces in der "Umgebung"
besuchen. Kandidaten sind
[u.A.](https://wiki.hackerspaces.org/List_of_Hacker_Spaces) [Das
Labor](https://das-labor.org/) in Bochum, das
[C4](https://koeln.ccc.de/c4/index.xml) in Köln und das
[Metalab](https://metalab.at/) in Wien. Will wer mitkommen?
