+++
title = "Smartphone Stativ-Halterung / Tripod Mount aus Foldback Clip"
date = 2016-02-01T09:11:42Z
author = "typ_o"
path = "/blog/2016/02/01/smartphone-stativ-halterung-tripod-mount-aus-foldback-clip"
aliases = ["/blog/archives/330-Smartphone-Stativ-Halterung-Tripod-Mount-aus-Foldback-Clip.html"]
+++
![](/media/cam-stativ.jpg)

Material:

- Foldback Clip 32 mm x 15 mm
- Multiplex 9,5 mm (Siebdruckplatte o.ä.)
- 1/4 Zoll Mutter
- Schrumpfschlauch

Den Clip mit zwei Flachzangen soweit überbiegen, dass der Anpressdruck
für das Fon nicht zu groß ist. Ein Loch für die Stativschraube mit
einem konischen Schleifer hineindremeln (Bohren ist wegen des gehärteten
Federstahls schwierig).

Das Holzklötzchen fräsen.
[smartphone-halter00.dxf](/media/smartphone-halter00.dxf "smartphone-halter00.dxf").
Der G-Code ist für einen 3 mm Fräser:
[smartphone-halter00.nc](/media/smartphone-halter00.nc "smartphone-halter00.nc").
Und hier noch das [Estlcam](https://www.estlcam.com/) - File dazu:
[smartphone-halter00.e25](/media/smartphone-halter00.e25 "smartphone-halter00.e25").
Das Klötzchen etwas abschrägen, damit es gut in den Clip paßt. Die
Mutter (Habe ich aus einer Webcam aus dem Kaffeeladen geerntet) in das
Klötzchen kleben (Zweikomponentenkleber).

Griffe aus dem Clip nehmen, Schrumpfschlauch über die Backen des Clips.
Mit einem Seitenschneider Löcher fur die Griffdrähte in den
Schrumpfschlauch knipsen, und die Griffe wieder einsetzen.
Zack-Bumm-Return: Fertig. Gesamtkosten \< 1 EUR.
