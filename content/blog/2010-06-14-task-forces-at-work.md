+++
title = "Task Forces at work"
date = 2010-06-14T19:00:54Z
author = "typ_o"
path = "/blog/2010/06/14/task-forces-at-work"
aliases = ["/blog/archives/92-Task-Forces-at-work.html"]
+++
Morgen um 19:00 [~~treffen~~](#) wir
uns alle wieder wie immer im Space. Die einzelnen Task Forces für
Gestaltung, Automation, Fundraising, Getränkesupport etc. legen jetzt
los, was man am steigenden Traffic auf der Mailingliste sehen kann.  
Die ersten kommen morgen ab 17:30, es geht um Renovierung der
Küchenecke, Planung des Tags der offenen Tür, der Linux -
Installationsparty, um die Elektroinstallation, ein Paper zur Ansprache
von Sponsoren, Inbetriebnahme der geschenkten Rechner, Weiterentwicklung
der Web-Steuerung für die Beleuchtung, und vieles mehr.

**Wenn du Interesse am Hackerspace hast komm dazu, guck die die Räume an
und was wir dort machen!** Wenn ihr Sachen habt, die ihr loswerden
wollt, Materialwunschliste anbei, Spendenquittung möglich!

## Geräte

- Getränkeautomat
- Durchlauferhitzer Unterbau, Strom
- Wasserkocher
- Mikrowelle
- Herd mit Backofen
- Industriestaubsauger
- Kaffemaschine mit Mahlwerk und allem Schnick und Schnuck
- Heißluftföns
- Ständerbohrmaschine
- Platinenbohrmaschine
- Stichsäge
- Schraubstock
- Platinenschere
- Platinen-Belichter, Entwickler, Ätzanlage
- Reflow - Ofen
- Beamer
- Leinwand
- Laserdrucker
- Elektro-Schweißgerät

## Material

- Leitungsschutzschalter
- RCDs
- Energiezähler mit S0-Ausgang, Drehstrom
- Steckdosen für Kabelkanal - Montage
- Kabelkanäle

## Möbel

- Brücken auf den Tischen für Ablage von Geräten
- Material-Regale (Schwerlast)
- Deckenlampen, Schreibtischlampen
- Stahlgitter (Baustahl-Armierung) von der Decke abhängen, da drauf und dran Verkabelung

## Infrastruktur

- Abfalleimer
- Spüle
- Wasser - Armatur
- Kücheneinrichtung
- Steckdosen auf Kabelkanal
- Unterverteilung mit elektronischem Arbeitszähler für
    Energieauswertung
- Server, Firewall, Router
- Labor-Status via AVR NET-IO auf der Website anzeigen (Anwesenheit
    detektieren \[datenschutz ftw!\], gif zum Einbinden erzeugen)\

## Ausstattung nichtelektrisch

- Sortieren:
  - Schäfer-Kästen
  - ESD-Kisten
  - Viele gleichgroße Pappschachteln ca. Schukarton Größe
- Whiteboard - Stifte
- Papier - Ablagefächer
- Geschirr 25 Sätze
- Besteck 25 Sätze
- Werkzeugsätze Elektronik / Mechanik
- Nicht alltägliches Werkzeug:
  - Bohrtisch
  - Heißdraht-Schneider

[Kontakt hier](https://flipdot.org/blog//archives/13-Communication.html).
