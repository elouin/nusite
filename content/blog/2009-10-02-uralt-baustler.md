+++
title = "Uralt - Baustler"
date = 2009-10-02T18:15:00Z
author = "typ_o"
path = "/blog/2009/10/02/uralt-baustler"
aliases = ["/blog/archives/15-Uralt-Baustler.html"]
+++
Die Herkünfte von [Hackerspaces](https://wiki.hackerspaces.org/) und
[Baustlern](https://bausteln.de/) liegen nicht nur im Do It Yourself und
Eisenbahn-Modellbau, ein Würzelchen ist auch Graswurzel(!)-und
Selbstversorger-Bewegung. Ein [Artikel der
taz](https://www.taz.de/1/archiv/print-archiv/printressorts/digi-artikel/?ressort=ku&dig=2009%2F09%2F28%2Fa0013&cHash=0f14ba5673)
portraitiert Christian Kuhtz, den Autor der Heftserie [Einfälle statt
Abfälle](https://www.einfaelle-statt-abfaelle.de/index.php). Die Texte,
detailliert wie [Instructables](https://www.instructables.com/), zeigen,
daß man alles selber machen kann: Käse, Öfen, Windräder und Fahrräder.
Alles sehr Low Tech und sustainable. Meine Bibel zu der Zeit war
"Energisch leben ...: Ein Handbuch der Alltagsökologie für
Selbstversorger".  
Was ist das Schönste an solchen Werken? Die Unbeirrbarkeit: Geht nicht -
gibts nicht. Und diese Unbeirrbarkeit liegt auch jedem Hack zugrunde.
