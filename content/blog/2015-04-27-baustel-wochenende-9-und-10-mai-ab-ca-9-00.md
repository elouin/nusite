+++
title = "Baustel - Wochenende 9. und 10. Mai ab ca. 9:00"
date = 2015-04-27T19:28:45Z
author = "typ_o"
path = "/blog/2015/04/27/baustel-wochenende-9-und-10-mai-ab-ca-9-00"
aliases = ["/blog/archives/276-Baustel-Wochenende-9.-und-10.-Mai-ab-ca.-900.html"]
+++
[![](/media/baustelwochenendepreview.serendipityThumb.jpg)](/media/baustelwochenendepreview.jpg)

Eine gemeinsame Aktion von
[BauKunstErfinden](https://www.baukunsterfinden.org/de/) und Flipdot.
[HIER](https://www.google.de/maps/place/51%C2%B019'18.3%22N+9%C2%B030'11.8%22E/@51.3217671,9.5033555,16z)
gehts rein.

Was ist ein Baustelwochenende?

Jeder der Lust hat, bringt was mit und arbeitet daran - und dann:
Fragen, über die Schulter gucken und mitmachen! **Eintritt frei.**

Bis jetzt gibt es:

- Raspberry Pi mit CAN Bus
- Robotik und Quadcopter
- EEG & Gehirn-Computer Schnittstellen mit OpenBCI
- ESP8266 C-Programmierung
- Beamer mit Camera (Videofeedback)
- Software: nodejs, HTML, angularjs, Linux, ...
- BulbCube ein Würfel aus Glühlampen
- Roboter
- 3D-Druck
- Raspberry Pi Web-Radio
- ESP8266 mit Lua

Mehr Infos [hier](https://flipdot.org/wiki/Baustelwochenende).  
Und ein
[Plakat](/media/Baustel_Plakat_kl.jpg).
[Unsere Kontaktseite](/kontakt/), oder direkt
mail: **com {kringeldings} flipdot punkt org**
