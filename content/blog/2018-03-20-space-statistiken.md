+++
title = "Space Statistiken"
date = 2018-03-20T08:53:51Z
author = "typ_o"
path = "/blog/2018/03/20/space-statistiken"
aliases = ["/blog/archives/398-Space-Statistiken.html"]
+++
Nach und nach wächst die Anzahl der visualisierten Messwerte aus dem
Space. Die öffentlichen Werte sind
[hier](https://stats.flipdot.org/) zu sehen.

Die CO2 Belastung:

[![](/media/CO2-Bildschirmfotovom2018-03-20091238.serendipityThumb.png)](/media/CO2-Bildschirmfotovom2018-03-20091238.png)

Der Space-Öffnungsgrad:

[![](/media/OPENING1-Bildschirmfotovom2018-03-20091701.serendipityThumb.png)](/media/OPENING1-Bildschirmfotovom2018-03-20091701.png)

[![](/media/OPENING2-Bildschirmfotovom2018-03-20091731.serendipityThumb.png)](/media/OPENING2-Bildschirmfotovom2018-03-20091731.png)

Energieumsatz (Regelmäßige kleine Spitzen: Kühlschrank):

[![](/media/POWER-.serendipityThumb.png)](/media/POWER-.png)

Heizung:

[![](/media/HEATER-Bildschirmfotovom2018-03-20091537.serendipityThumb.png)](/media/HEATER-Bildschirmfotovom2018-03-20091537.png)

Traffic im (non public) Forum:

[![](/media/FORUM-Bildschirmfotovom2018-03-20091328.serendipityThumb.png)](/media/FORUM-Bildschirmfotovom2018-03-20091328.png)

Personen (DHCP Leases) im Space:

[![](/media/USERS-.serendipityThumb.png)](/media/USERS-.png)
