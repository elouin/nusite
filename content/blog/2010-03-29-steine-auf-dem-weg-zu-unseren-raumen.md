+++
title = "Steine auf dem Weg zu unseren Räumen"
date = 2010-03-29T16:59:59Z
author = "typ_o"
path = "/blog/2010/03/29/steine-auf-dem-weg-zu-unseren-raumen"
aliases = ["/blog/archives/80-Steine-auf-dem-Weg-zu-unseren-Raeumen.html"]
+++
Letzte Woche noch waren die unterschriftsreifen Verträge fast auf dem
Tisch, mittlerweile steht die Vermietung seitens der Deutschen Bahn
plötzlich in Frage.

Wie beurteilen wir die Lage am Hbf? Wie wollen wir weiter vorgehen?
Tragen es alle mit, die Beiträge auf die Hohe Kante zu legen? Wie
könnten Alternative aussehen? Welche Deadlines wollen wir uns setzen?

Das und mehr Dienstag 19:00 im
[Kunsttempel](https://flipdot.org/blog/archives/47-Ab-jetzt-immer-Dienstags.html).
