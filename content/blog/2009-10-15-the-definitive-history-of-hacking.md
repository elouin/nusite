+++
title = "The Definitive History of Hacking"
date = 2009-10-15T01:00:00Z
author = "typ_o"
path = "/blog/2009/10/15/the-definitive-history-of-hacking"
aliases = ["/blog/archives/33-The-Definitive-History-of-Hacking.html"]
+++
Auf
[WonderHowTo](https://www.wonderhowto.com/wonderment/the-definitive-history-of-hacking-0113408/).
50 minute documentary on the history of hacking by the Discovery
Channel. Hacking is about more than modding electronics or "hacking"
Google. Learn about where it all began.  
(Jaja, jede Menge Klischees, trotzdem lustig)
