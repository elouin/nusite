+++
title = "SDR online"
date = 2013-09-20T20:28:36Z
author = "typ_o"
path = "/blog/2013/09/20/sdr-online"
aliases = ["/blog/archives/216-SDR-online.html"]
+++
[Software Defined
Radio](https://de.wikipedia.org/wiki/Software_Defined_Radio) online
hören, eine [Installation](https://websdr.ewi.utwente.nl:8901/) der
[University of Twente](https://www.utwente.nl/). Meist sind ca. 150 (!)
Hörer auf der Maschine. Im Chatfenster gehts oft um
[Zahlensender](https://de.wikipedia.org/wiki/Zahlensender).

Hier eine Aufnahme des Senders E06 vom 20.9.2013 um 08:18 h von mir, mit
der oben erwähnten Seite:
[Zahlensender\_14830\_khz\_Kennung\_E06\_websdr-recording.mp3](/media/Zahlensender_14830_khz_Kennung_E06_websdr-recording.mp3 "Zahlensender_14830_khz_Kennung_E06_websdr-recording.mp3").
Jede Menge Material, auch zu diesem Sender
[hier](https://www.simonmason.karoo.net/page30.html).

"[Achtung: Fünnef, zwo, null, null,
Trennung...](https://www.swldxer.co.uk/dlf.wma)", Feature vom
Deutschlandfunk dazu.

Ausschnitt aus "Der Westen leuchtet":
