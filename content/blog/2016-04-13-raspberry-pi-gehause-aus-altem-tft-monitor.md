+++
title = "Raspberry Pi Gehäuse aus altem TFT Monitor"
date = 2016-04-13T07:43:00Z
author = "typ_o"
path = "/blog/2016/04/13/raspberry-pi-gehause-aus-altem-tft-monitor"
aliases = ["/blog/archives/341-Raspberry-Pi-Gehaeuse-aus-altem-TFT-Monitor.html"]
+++
In Monitoren findet man oft ca. [6 mm starke
Acrylglasscheiben](https://de.wikipedia.org/wiki/Datei:Tft_innenleben.jpg),
die mit einem Punktmuster zur homogenen Verteilung der an den Seiten
eingestrahlten Beleuchtung versehen sind.  
Ein erster Versuch zeigte, dass sich das Material mit einem
Einzahnfräser gut zerspanen lässt. (3 mm Fräser, ca. 6000 upm, 400 mm / min
Vorschub, 3 mm Zustellung, Kühlung mit Wasser + ca. 1/5 Spiritus).

[![](/media/PiCase01.serendipityThumb.jpg)](/media/PiCase01.jpg)  
Hier die Herstellungsdaten:
[LCDAcrylgehäusePi2.dxf](/media/LCDAcrylgehusePi2.dxf "LCDAcrylgehusePi2.dxf"),
[LCDAcrylgehäusePi2.nc](/media/LCDAcrylgehusePi2.nc "LCDAcrylgehusePi2.nc"),
[LCDAcrylgehäusePi2.e25](/media/LCDAcrylgehusePi2.e25 "LCDAcrylgehusePi2.e25")

[![](/media/PiCase02.serendipityThumb.jpg)](/media/PiCase02.jpg)
