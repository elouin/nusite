+++
title = "Woher und wohin"
date = 2009-09-11T13:20:00Z
author = "typ_o"
path = "/blog/2009/09/11/woher-und-wohin"
aliases = ["/blog/archives/3-Woher-und-wohin.html"]
+++
Tim Pritlove (Moderation), Astera, Johannes Grenzfurthner vom Meta Lab
zu **Hackerspaces** im [Chaosradio Express Podcast
Nr. 134](https://chaosradio.ccc.de/cre134.html).
