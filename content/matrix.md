+++
title = "Matrix"
+++

Wir nutzen zur Echtzeitkommunikation hauptsächlich Matrix. Verbinde dich gerne zu unseren digitalen
Sofaecke um zu quatschen: [#general:flipdot.org](https://matrix.to/#/#general:flipdot.org)

Wenn du lieber redest als zu schreiben, schau doch mal in unserm [Mumble](/mumble/) vorbei.

## Alternative Verbindungsarten

### IRC

Wenn du dich lieber per IRC verbinden willst, komm gern [hier](ircs://irc.libera.chat:6697) vorbei:

```
Server: irc.libera.chat
Port:   6697 (TLS)
Raum:   #flipdot
```

Auf diesen IRC-Kanal werden die Nachrichten von Matrix aus gebridget und umgekehrt. Dank Puppeting sind Nutzer, die via Matrix oder auch Telegram verbunden sind sichtbar. Sei dir aber bitte bewusst, dass der IRC eventuell mühsamer zu nutzen ist als Matrix direkt, z.B. da längere Nachrichten als Link zu einer Textnachricht verschickt werden. Das Netzwerk, das wir nutzen, heißt [Libera.Chat](https://libera.chat/).

### Telegram

Telegram ist ein nicht-dezentraler Dienst dessen Server-Komponente proprietäre Software ist. Somit ist sie aus Sicht der [Hackerethik](https://www.ccc.de/de/hackerethik) im Sinne des verweigerten unbegrenzten Zugangs und der Förderung von Dezentralisierung abzulehnen. Wenn du ihn dennoch nutzen willst kannst du der Telegram-Gruppe *flipdot_kassel* beitreten. Sei dir aber bitte bewusst, dass Telegram eventuell mühsamer zu nutzen ist als Matrix direkt, z.B. da Nutzer ohne Telegram-Account nicht direkt erkennbar sind und somit als Nachricht eines Bots unleserlich sein könnten.
