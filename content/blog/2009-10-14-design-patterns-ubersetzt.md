+++
title = "Design Patterns übersetzt"
date = 2009-10-14T01:00:00Z
author = "typ_o"
path = "/blog/2009/10/14/design-patterns-ubersetzt"
aliases = ["/blog/archives/38-Design-Patterns-uebersetzt.html"]
+++
Die bereits
[verlinkten](https://flipdot.org/blog/archives/2-Erst-Ei,-dann-Gack!.html)
Design Patterns for Hackerspaces hier [zusammengefaßt auf
Deutsch](https://futur.plomlompom.de/archiv/1864/24c3-4-wir-designen-einen-hackerspace).
