+++
title = "Vortrag - Zelluläre Automaten"
date = 2020-02-05T01:06:01Z
author = "Baustel"
path = "/blog/2020/02/05/vortrag-zellulare-automaten"
aliases = ["/blog/archives/435-Vortrag-Zellulaere-Automaten.html"]
+++
2020-02-11 um 20:00 Uhr im Space **Zelluläre Automaten**

![](/media/anim.gif)

- **Kein Vorwissen benötigt**
- Conway's Game of Life
- 2D Zelluläre Automaten
- Entdeckung neuer Regeln
- Interaktive Software
- Technik kann Kunst und Schönheit schaffen

Gäste und Interessierte sind herzlich willkommen, Eintritt ist frei.

[Folien
herunterladen](https://s.flx.ai/2020/cellular-automata/ "Folien herunterladen")
