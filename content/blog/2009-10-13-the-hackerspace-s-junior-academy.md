+++
title = "The Hackerspace's Junior Academy"
date = 2009-10-13T01:00:00Z
author = "typ_o"
path = "/blog/2009/10/13/the-hackerspace-s-junior-academy"
aliases = ["/blog/archives/24-The-Hackerspaces-Junior-Academy.html"]
+++
U23 Köln - Workshops für junge Leute in Hackerspaces organisieren.
[Präsentation vom: 27.12.2008, fd0, Lars
Weiler](https://chaosradio.ccc.de/25c3_m4v_2827.html).
