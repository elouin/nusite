+++
title = "Raspberry Pi als Desktop-PC"
+++

**Raspberry Pi 3/4 als Desktop Computer, Overclocked mit Kühlkörper,
Zutaten:**

Schnelle(!) SD-Karte, auch für viele kleine OS Zugriffe. z.B.:

- SAMSUNG EVO plus (2017+)

OS:

! Bei Pi4 geht nur ein HDMI Anschluss während der Installation, der
andere zeigt nur das "Regenbogenbild"

- Dualboot für Raspberry Linux "Raspian" und Mediaplayer "Kodi"
  oder andere unterstützte OS: <https://www.raspberrypi.org/downloads/noobs>  
  Inhalt des gezippten Ordners entpackt auf leere SD-Karte kopieren
- Raspian alleine in verschiedenen Austattungen: <https://www.raspberrypi.org/downloads/>  
  Balena Etcher runterladen und nutzen um Image auf SD-Karte zu bringen, oder Anleitung.

How to Overclock:

- [Pi3](/projekte/raspberry-pi/raspberry-pi-3-overclock/)
- [Pi4](/projekte/raspberry-pi/raspberry-pi-4-overclock/)

Bildschirm Adapter:

- Pi3  
  Für alte VGA-Bildschirme an dem Pi3 reicht ein normaler HDMI zu VGA Adpater
  für 3-5 Euro aus ähnlich diesem <https://www.amazon.de/dp/B07T14DXN5/>
- Pi4 (kann zwei Bildschirme)  
  Für den pi4 brauch man zwei von obigen Adapternn und noch zusätzlich je Bildschirm
  - 1x micro Hdmi zu HDMI Adapterkabel und <https://www.idealo.de/preisvergleich/OffersOfProduct/4426151_-hdmi-auf-micro-hdmi-logilink.html>
  - 1x Buchse-Buchse HDMI Adapater <https://www.idealo.de/preisvergleich/OffersOfProduct/1873250_-65049-adapter-hdmi-buchse-buchse-delock.html>
