+++
title = "flipdot Hackspace Kassel"
extra.tagline = "CCC-Erfa, Hackspace"
extra.location = "Kassel, [beim Hauptbahnhof](/kontakt/#Anfahrt)"
+++

flipdot versteht sich als einen Ort, der Menschen einen Raum gibt, sich zwanglos zu treffen, sich
auszutauschen und vielleicht zu neuen, anderen Ideen zu kommen. Hier werden Projekte durchdacht und
umgesetzt, aber auch gemeinsam gegessen und geplaudert.
