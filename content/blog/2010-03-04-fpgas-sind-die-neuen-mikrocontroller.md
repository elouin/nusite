+++
title = "FPGAs sind die neuen Mikrocontroller?"
date = 2010-03-04T18:09:00Z
author = "typ_o"
path = "/blog/2010/03/04/fpgas-sind-die-neuen-mikrocontroller"
aliases = ["/blog/archives/76-FPGAs-sind-die-neuen-Mikrocontroller.html"]
+++
Heute auf der [Messe](https://www.embedded-world.de/de/default.ashx) auf
der Suche nach
[FPGA](https://de.wikipedia.org/wiki/Field_Programmable_Gate_Array)-Entwicklungsboards.
Hier ein Fund für \<50EUR, auf dem Baustein befindet sich ein
[Spartan 3A](https://www.xilinx.com/support/documentation/data_sheets/ds529.pdf)
Chip von [Xilinx](https://en.wikipedia.org/wiki/Xilinx), dessen kleinste
Ausführung mal eben 100.000 Logikgatter hat. Wär' Das nicht ein schönes
Spielzeug? Programmiert wird das mit einer frei verfügbaren
Entwicklungsumgebung, dem
[WebPack](https://www.xilinx.com/support/download/index.htm).
