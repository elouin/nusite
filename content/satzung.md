+++
title = "Satzung"

# Custom template for custom CSS
template = "pages/satzung.html"
+++

<div class="satzung">

Beschlossen am 19.04.2020

§ 01 Name, Sitz, Rechtsfähigkeit, Geschäftsjahr

1. Der Verein führt den Namen "flipdot".
2. Sitz des Vereins ist Kassel.
3. Der Verein soll in das Vereinsregister des zuständigen Amtsgerichts
   eingetragen werden; nach der Eintragung führt er den Zusatz "e.V.".
4. Geschäftsjahr des Vereins ist das Kalenderjahr.

§ 02 Zweck des Vereins

1. Der Verein verfolgt ausschließlich und unmittelbar gemeinnützige Zwecke
   im Sinne des Abschnitts "Steuerbegünstigte Zwecke" der Abgabenordnung.
2. Zweck des Vereins ist die Förderung von Wissenschaft und Forschung,
   Kunst und Kultur sowie der Bildung und Völkerverständigung.
3. Der Satzungszweck wird insbesondere verwirklicht durch
   * Anregung und Unterstützung wissenschaftlicher und künstlerischer
     Arbeiten zu technischen Themen;
   * Vermittlung von technischen Kenntnissen auf Veranstaltungen und
     durch Veröffentlichungen;
   * Internationalen Austausch über wissenschaftliche und künstlerische
     Arbeiten sowie internationale Veröffentlichungen.
4. Der Verein ist selbstlos tätig, er verfolgt nicht in erster Linie
   eigenwirtschaftliche Zwecke.

§ 03 Vereinsmittel

2. Mittel des Vereins dürfen nur für satzungsgemäße Zwecke verwendet werden.
    Die Mitglieder erhalten keine Zuwendungen aus Mitteln des Vereins.
3. Es darf keine Person durch Ausgaben, die dem Zweck der Körperschaft fremd
    sind, oder durch unverhältnismäßig hohe Vergütungen begünstigt werden.

§ 04 Mitgliedschaft

Mitglieder können natürliche Personen und juristische Personen sein.
Natürliche Personen, die in ihrer Geschäftsfähigkeit beschränkt sind, können
nur mit Zustimmung des gesetzlichen Vertreters Mitglied werden.

§ 05 Beginn und Ende der Mitgliedschaft

1. Die Mitgliedschaft wird durch eine Beitrittserklärung in Textform gegenüber
   einem Vorstandsmitglied begründet.
2. Der Vorstand kann den Beitritt innerhalb von drei Monaten durch Beschluss
   ablehnen. In diesem Fall gilt die Mitgliedschaft als von Anfang an nicht
   zustande gekommen. Die Ablehnung ist dem Betroffenen von einem
   Vorstandsmitglied mitzuteilen. Eine Begründung ist nicht erforderlich.
   Während dieser Zeit ist das Mitglied nicht stimmberechtigt.
3. Die Mitgliedschaft endet
   * bei juristischen Personen mit deren Auflösung;
   * bei natürlichen Personen mit ihrem Tod;
   * durch Austritt. Der Austritt wird wirksam zum Ende des Kalendermonats
     nach Zugang der Austrittserklärung. Es genügt die Textform;
   * automatisch bei Mitgliedern, die sich mit mehr als drei Monatsbeiträgen
     im Verzug befinden;
   * bei Ausschluss des Mitgliedes.

§ 06 Mitgliedsbeiträge

Die Mitglieder entrichten Mitgliedsbeiträge nach der durch die
Mitgliederversammlung festgelegten [Beitragsordnung](/beitragsordnung/).

§ 07 Organe

Die Organe des Vereins sind die Mitgliederversammlung, der Vorstand und der
Kassenwart.

§ 08 Mitgliederversammlung

1. Die Mitgliederversammlung besteht aus den Mitgliedern des Vereins.
   In der Versammlung hat jedes Mitglied eine Stimme.
2. Die ordentliche Mitgliederversammlung wird einmal jährlich vom Vorstand
   in Textform einberufen.
3. Außerordentliche Mitgliederversammlungen werden entweder auf Beschluss des
   Vorstandes oder auf ein entsprechendes Verlangen eines Zehntels der
   stimmberechtigten Mitglieder gegenüber dem Vorstand hin einberufen.
4. Die Einladung zur Mitgliederversammlung an die Mitglieder erfolgt in
   Textform unter Angabe von Ort, Zeit und Tagesordnung mindestens zwei Wochen
   im Voraus.
5. Anträge zur Tagesordnung durch Mitglieder müssen spätestens eine Woche vor
   der Versammlung an alle Mitglieder in Textform gestellt werden.
6. An einer Mitgliederversammlung kann elektronisch teilgenommen werden,
   sofern geeignete technische Mittel vorhanden sind.
7. Die Mitgliedervesammlung ist beschlussfähig, wenn mindestens ein Zehntel
   der stimmberechtigten Mitglieder teilnehmen.
8. Ist die Mitgliederversammlung nicht beschlussfähig, wird eine
   Wiederholungsversammlung einberufen, die in jedem Falle beschlussfähig ist.
   Darauf ist in der Einladung zur Wiederholungsversammlung hinzuweisen.

§ 09 Zuständigkeiten der Mitgliederversammlung

Die Mitgliederversammlung
1. wählt und kontrolliert den Vorstand und den Kassenwart,
2. prüft und genehmigt die Jahresabschlussrechnung des Kassenwartes und
   erteilt die Entlastung von Kassenwart und Vorstand,
3. entscheidet in allen Fällen, in denen nicht die Zuständigkeit eines
    anderen Organes bestimmt ist,
4. trifft Entscheidungen, wobei für die Annahme einer Entscheidung eine
    einfache Mehrheit notwendig ist,
5. wird von einem zu Beginn der Mitgliederversammlung bestimmten
   Protokollanten schriftlich protokolliert und von ihm und von zwei
   Vorstandsmitgliedern unterzeichnet.
6. kann sich eine Geschäftsordnung geben.

§ 10 Vorstand

1. Der Vorstand gemäß § 26 BGB besteht aus 3 Mitgliedern und wird auf 2 Jahre
   durch die Mitgliederversammlung gewählt. Vorstand können nur natürliche
   Personen sein. Ein Vorstandsmitglied wird von der Mitgliederversammlung
   als Kassenwart gewählt. Die Wahl der Vorstandsmitglieder erfolgt einzeln
   und in geheimer Abstimmung. Eine Wiederwahl ist möglich.
2. Jeweils zwei Vorstandsmitglieder vertreten den Verein.
3. Zu Sitzungen des Vorstandes ist eine Woche vorher in Textform einzuladen.
   Mit dem Einverständnis aller Mitglieder des Vorstandes
   kann diese Frist verkürzt werden oder ganz entfallen.
4. Der Vorstand ist beschlussfähig, wenn mindestens 3 Mitglieder des
   Vorstandes an der Sitzung teilnehmen.
5. An einer Vorstandssitzung kann elektronisch teilgenommen werden, sofern
   geeignete technische Mittel vorhanden sind.
6. Beschlüsse im Vorstand werden mit einfacher Mehrheit gefasst.
7. Der Vorstand ist ermächtigt, gerichtlich oder behördlich geforderte
   Satzungsänderungen bis zur nächsten Mitgliederversammlung durchzuführen
   und umzusetzen.

§ 11 Zuständigkeiten des Vorstandes

1. Der Vorstand führt die Geschäfte des Vereins und fasst die
   erforderlichen Beschlüsse.
2. Dem Kassenwart obliegt die Führung von Aufzeichnungen über Ausgaben und
   Einnahmen des Vereins.
3. Jedes Vorstandsmitglied ist ermächtigt, für den Verein im Außenverhältnis
   Rechtsgeschäfte bis zu einem Wert von 500 € abzuschließen.
   Der Vorstand ist ermächtigt, Rechtsgeschäfte bis zu einem Wert von 2000 €
   abzuschließen.
   Über diesen Wert hinaus ist ein Beschluss der Mitgliederversammlung gemäß
   § 9 Abs. 4 und Abs. 5 erforderlich.

§ 12 Ausschluß eines Mitgliedes

1. Der Vorstand kann mit einfacher Mehrheit ein Mitglied auf Antrag
   ausschließen.
2. Gegen den Ausschluß kann Widerspruch eingelegt werden.
3. Ein Widerspruch führt zu einer Überprüfung des Ausschlusses durch die
   Mitgliederversammlung. Die einfache Mehrheit muss den Ausschluss
   bestätigen.
4. Bis zur Entscheidung der Mitgliederversammlung ruht die Mitgliedschaft.

§ 13 Auflösung

1. Zur Auflösung des Vereins bedarf es der Dreiviertelmehrheit der an der
    Mitgliederversammlung teilnehmenden Mitglieder.
2. Bei Auflösung des Vereins oder bei Wegfall steuerbegünstigter Zwecke
    fällt das Vermögen des Vereins an eine vergleichbare steuerbegünstigte
    Körperschaft zur Förderung von Wissenschaft und Forschung,
    Kunst und Kultur, der Bildung oder der Völkerverständigung.

§ 14 Sonstiges

1. Beschlüsse, durch die eine für steuerliche Vergünstigungen wesentliche
    Satzungsbestimmung geändert, ergänzt, in die Satzung eingefügt oder
    aufgehoben wird oder die Auflösung des Vereins, die Überführung in eine
    andere Körperschaft oder die Übertragung des Vereinsvermögens als Ganzes
    sind der zuständigen Finanzbehörde durch den Vorstand unverzüglich
    mitzuteilen.
2. Vor der Verteilung oder Übertragung des Vereinsvermögens ist die
    Unbedenklichkeitserklärung des zuständigen Finanzamtes einzuholen.

§ 15 Inkrafttreten

Die Satzung tritt mit Eintragung in das Vereinsregister in Kraft.

</div>
