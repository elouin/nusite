+++
title = "Sonntag: Zum digitalen Datum in den Hackerspace"
date = 2010-10-07T21:01:47Z
author = "typ_o"
path = "/blog/2010/10/07/sonntag-zum-digitalen-datum-in-den-hackerspace"
aliases = ["/blog/archives/107-Sonntag-Zum-digitalen-Datum-in-den-Hackerspace.html"]
+++
Wer sich bisher noch nicht zum <a href="#"><del>öffentlichen Treffen</del></a> vom
[flipdot - hackerspace kassel](https://web.archive.org/web/20120105031721/https://flipdot.org/wiki/index.php?title=Hallo!)
(an jeden Dienstag Abend) getraut hat, weil ihm VPN, IPSEC, ISP und FUBAR zu
[kryptische
Akronyme](https://de.wikipedia.org/wiki/Liste_von_Abk%C3%BCrzungen_%28Netzjargon%29)
sind, hat am Sonntag dem 10.10.10 Gelegenheit, sich am Datum 101010 die
Antwort zum Leben, dem Universum und dem ganzen Rest erklären zu lassen.
Es gibt einen Workshop zur Herstellung des wichtigsten Nahrungsmittels
für Nerds und Musique live aus der Konserve. Ab ca. 15:00 in der
[Sickingenstraße 10](/kontakt/#anfahrt) in
Kassel.

Edit: Und ja, man kann Pizza selbst machen, allerdings hat der Teig- und
Backprozess doch so einige Kontrollparameter...

![](/media/0teig.serendipityThumb.jpg)![](/media/1teig.serendipityThumb.jpg)![](/media/2teig.serendipityThumb.jpg)![](/media/3dose.serendipityThumb.jpg)![](/media/4herd.serendipityThumb.jpg)![](/media/5pizza.serendipityThumb.jpg)![](/media/6essen.serendipityThumb.jpg)
