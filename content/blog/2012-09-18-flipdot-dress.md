+++
title = "FlipDot Dress"
date = 2012-09-18T19:46:38Z
author = "typ_o"
path = "/blog/2012/09/18/flipdot-dress"
aliases = ["/blog/archives/173-FlipDot-Dress.html"]
+++
Coole Idee, in Zusammenarbeit der Künstlerin mit dem MetaLab entstanden:
Eine [Weste mit
Flipdot-Elementen](https://www.paulinevandongen.nl/eng/2012/flip-dot-dress-the-process/)!
