+++
title = "All Creatures Welcome"
date = 2019-01-04T15:11:20Z
author = "typ_o"
path = "/blog/2019/01/04/all-creatures-welcome"
aliases = ["/blog/archives/425-All-Creatures-Welcome.html"]
+++
[![](/media/acw_poster_ki_regenbogen_digital.serendipityThumb.jpg)](/media/acw_poster_ki_regenbogen_digital.jpg)

In Zusammenarbeit mit den Balikinos in Kassel laden wir zur [Matinee am
13. Januar 2019 um
12:00](https://www.balikinos.de/index.php?id=-1&uid=4549 "Balikinos")
ein.

**All Creatures Welcome** - der Film über das Mindset der Menschen im
und um den Chaos Computer Club.  

<div style="padding: 56.25% 0 0 0;">

</div>

[ALL CREATURES WELCOME - Official Trailer](https://vimeo.com/196339260)
from [Sandra Trostel](https://vimeo.com/sandratrostel) on
[Vimeo](https://vimeo.com).

Mit dem Aufruf „use hacking as a mindset“ tauchen wir zusammen mit der
Filmemacherin in eine Art dokumentarisches Adventure Game ein und
erforschen die Welt der „Komputerfrieks“, wie sie sich beim
Gründungstreffen des Chaos Computer Clubs (kurz CCC) 1981 noch selbst
nannten. Wir entdecken dabei eine offene, freigeistige Gesellschaft. Die
Veranstaltungen des CCC sind eine Art utopistisches Realweltabbild des
virtuellen Spektrums. Wir stoßen auf Engel, Aktivisten, Roboter und
Furries, Coder und Maker. Wir erfahren, was ein Hack ist, warum
Lockpicking wichtig sein kann, wie ein zelluläres System funktioniert.
Wir tauchen in das Spiel ein und werden selbst Teil der Community, die
ebenso inklusiv wie anspruchsvoll agiert.
