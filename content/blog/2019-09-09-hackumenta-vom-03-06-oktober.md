+++
title = "Hackumenta vom 03. - 06. Oktober"
date = 2019-09-09T21:41:41Z
author = "Baustel"
path = "/blog/2019/09/09/hackumenta-vom-03-06-oktober"
aliases = ["/blog/archives/430-Hackumenta-vom-03.-06.-Oktober.html"]
+++
![Hackumenta
Flyer](/media/hackumenta_2019_flyer_small.png)

**flipdot ist seit 2009 der Kasseler Hackerspace und seit 2016 lokaler
Chaos Computer Club Erfahrungsaustauschkreis.**

Wir sind knapp 60 Mitglieder zwischen 14 und 64 Jahren. Jeder ist
willkommen, unabhängig von Interessen und Fähigkeiten. Wenn du Interesse
an Technik, Bausteln, Hacken oder Coden hast, gerne Fragen stellst und
beantwortest, sei willkommen!

Wir laden Dich ein, uns zu unserer Veranstaltung Hackumenta – 10 Years
in Space zu besuchen. Hier kannst du unseren Space besichtigen,
Workshops und Vorträge besuchen, leckere Sachen essen, zu live gemixter
Musik tanzen, und [realisierte und entstehende
Projekte](/projekte/) besichtigen.

Mehr Informationen unter [0xa.flipdot.org](https://2019.hackumenta.de/)

Übrigens: Wenn dir der Flyer nicht gefällt, [generier' dir halt 'nen
Neuen!](https://flipdot.github.io/0xA-flyer-generator/)
