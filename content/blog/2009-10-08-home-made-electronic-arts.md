+++
title = "Home Made Electronic Arts"
date = 2009-10-08T10:44:35Z
author = "typ_o"
path = "/blog/2009/10/08/home-made-electronic-arts"
aliases = ["/blog/archives/36-Home-Made-Electronic-Arts.html"]
+++
[Philip Steffan](https://bausteln.de/kontakt/) twittert, er [fand
grad](https://twitter.com/bausteln/statuses/4705261517) ein Buch, das ich
nicht kannte, obwohl Olaf da einen Beitrag drin hat: [Home Made
Electronic
Arts](https://www.merianverlag.ch/buecher/detail.cfm?ObjectID=5B23A74F-1422-0CEF-B45B1F9DC99E47EE),
Do-it-yourself Piratensender, Krachgeneratoren und Videomaschinen
