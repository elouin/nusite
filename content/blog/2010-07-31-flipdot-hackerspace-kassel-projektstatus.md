+++
title = "flipdot hackerspace kassel Projektstatus"
date = 2010-07-31T05:50:09Z
author = "typ_o"
path = "/blog/2010/07/31/flipdot-hackerspace-kassel-projektstatus"
aliases = ["/blog/archives/99-flipdot-hackerspace-kassel-Projektstatus.html"]
+++
Unsere Räume haben wir chaostypisch dann doch ohne
Innenarchitektur-Planung eingerichtet, so steht jetzt erstmal alles auf
seinem Platz. Es gibt eine Chillout-Ecke mit Liegestuhl und blauem
Himmel, in der Küche ist der Geschirrspüler angeschlossen, und die von
Jürgen geplanten und gezeichneten Arbeits- und Tresenflächen aus
Multiplex-Platten kommen in wenigen Tagen.

Die [Infrastruktur](/infrastruktur/)
migriert unter den Händen der Netzwerk-Taskforce Arazer und Induzer
langsam von "extrem fliegend aufgebaut" zu "fest installiert", und
sobald die erwarteten Switche und Router da sind, steht der grobe
Rahmen. Für lichtflüchtende Lebensformen gibt es eine dunklere Ecke mit
viel Bildschirmen, Netz und Platz, die auch schon mehrmals bis in die
Morgendämmerung benutzt wurde. Im Hardwarebereich installiert Burkhard
Brüstungskanäle mit viel Strom und Netzwerk drin, damit die noch bei
Helmut zu Hause bereit stehenden Laborgeräte (Ein Stapel Hameg Oszi,
DMM, Zähler und Generatoren), angeschlossen werden können.

Der Zugang in den space mit den vier Schlüsselsätzen und Codes für alle
Türen, Gitter und Brandschutz-Abschnitte funktioniert ganz flüssig,
dennoch arbeiten wir an einer Lösung, dass alle Member ohne Absprache
24/7 Zugang haben können.

Sobald die Ferienzeit herum ist, werden wir mit unserem
Veranstaltungsprogramm beginnen, auf dem Plan steht unter anderem
Netzwerktechnik und Security, µC-Grundlagen und Fragen zur Allmende und
Open Source. Auch ein weiteres Baustelwochenende ist im Gespräch.

Spenden kamen und kommen aus verschiedenen Richtungen, bis jetzt von
SMA, Conrad, CL-Bergmann, die Firmen Werkzeug Wermas, Watterott und
Fritz macht Licht haben bereits Hilfen zugesagt. Wir freuen uns über die
vielen guten Materialien, jetzt fehlt noch eine dauerhafte Finanzierung
der Räumlichkeiten.

Mit der Anmietung der 150 m² für den space haben wir uns mutig aus dem
Fenster gelehnt, in der Hoffnung, innerhalb eines halben Jahres genug
Menschen beisammen zu haben, die Lust auf einen unabhängigen und freien
Ort zum Coden, Reden, Löten, Schrauben und Planen haben - und denen das
einen regelmäßig Beitrag wert ist. [Der Tag der offenen
Tür](/media/slideshows/offtuer2010-06/) war
gut besucht, inzwischen sind wir fast 20 Member, die stabil zum Projekt
beitragen, nicht alle mit dem vollen Beitragssatz, manche vor allem mit
persönlichem Einsatz. Damit die Miete drin ist, fehlen jetzt nur noch
ca. fünf weitere Member - und das schaffen wir auf jeden Fall!

Wenn du einfach mal reinschauen willst - jeden Dienstag ab 19:00 ist
Gelegenheit dazu!
