+++
title = "Starttreffen"
date = 2009-10-06T04:20:11Z
author = "typ_o"
path = "/blog/2009/10/06/starttreffen"
aliases = ["/blog/archives/30-Starttreffen.html"]
+++
Sammlung zu den Themen heute Abend
<a href="#"><del>hier</del></a>, pse contribute.

**Edit:**  
Das erste Treffen war (für mich) eine große Überraschung: 28 Teilnehmer
mit *so* vielen Zielen und Ideen, daß man sich fragt: "Warum erst
jetzt?". In einer Woche treffen wir uns in den bereits in Frage
kommenden Räumlichkeiten, und führen anschließend die Themen weiter, die
wir heute Abend nur anreißen konnten. (Details in der
<a href="#"><del>Mailingliste</del></a>)
