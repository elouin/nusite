+++
title = "Besuch im Open Lab in Augsburg"
date = 2016-03-22T21:50:35Z
author = "typ_o"
path = "/blog/2016/03/22/besuch-im-open-lab-in-augsburg"
aliases = ["/blog/archives/337-Besuch-im-Open-Lab-in-Augsburg.html"]
+++
Bei den Mönchen soll es so gewesen sein: Wenn die von Kloster zu Kloster
reisten, fühlten sie sich sofort wieder wie zu Hause, weil bei ihren
Ordensbrüdern am fremden Ort alles bekannt und vertraut war.

So ging es mir heute beim Besuch im [Open Lab
Augsburg](https://openlab-augsburg.de/). Ein einladender Makerspace in
Bahnhofsnähe, viel Hardwaregebastel, eine entstehende Holz /
Mechanik-Werkstatt, sehr gute Räume, und wie überall in unserem Orden:
Gesprächsbereite, freundliche, interessierte Menschen. Wie schön!

Es sind die gemeinsamen Werte, die mich gefangen nehmen: Machen ist
interessanter als darüber reden, reparieren besser als neu kaufen,
lernen wichtiger als konsumieren.

Den Geheimtipp für den Industrietechnik-Schrottplatz kann ich leider
mangels Zeit nicht mehr nutzen - aber nächstes Mal bestimmt!

[![](/media/20160322_211349.serendipityThumb.jpg)](/media/20160322_211349.jpg)
