+++
title = "Coronavirus: Space bleibt für alle geschlossen"
date = 2020-03-14T14:23:31Z
author = "Baustel"
path = "/blog/2020/03/14/coronavirus-space-bleibt-fur-alle-geschlossen"
aliases = ["/blog/archives/439-Coronavirus-Space-bleibt-fuer-alle-geschlossen.html"]
+++
Aufgrund der aktuellen Situation ist der flipdot bis zum **30. April
*nicht*** wie üblich am Dienstag für Besucher geöffnet. Wir empfehlen
Mitgliedern mit Schlüsselzugang, ihre Besuche auf ein Minimum zu
reduzieren.

Vielen Dank für euer Verständnis.

**Update \[16.03.2020\]:** Auch Mitgliedern ist der Zugang zum Space
untersagt, die digitalen Schlüssel wurden deaktiviert. [Mitteilung auf
bundesregierung.de](https://www.bundesregierung.de/breg-de/aktuelles/vereinbarung-zwischen-der-bundesregierung-und-den-regierungschefinnen-und-regierungschefs-der-bundeslaender-angesichts-der-corona-epidemie-in-deutschland-1730934)
