+++
title = "Besuch von der Uni"
date = 2010-10-20T05:00:00Z
author = "typ_o"
path = "/blog/2010/10/20/besuch-von-der-uni"
aliases = ["/blog/archives/114-Besuch-von-der-Uni.html"]
+++
Grade hatten wir Besuch vom Fachbereich Architektur, Stadt- und
Landschaftsplanung: Florian Gwinner und Karen Winzer berichteten von
ihrem Projekt: [MACH’S DIR
SELBST](https://www.atelierk10.de/?programm&WS_201011). Sie wollen in
kleinen Gruppen High-Tech-Low-Budget-Maschinen wie ein 3D-Scanner, ein
3D-Plotter, eine CNC-Fräse, ein Beamer, ein Mikrokopter, eine Solarzelle
zunächst selbst bauen und dann direkt anwenden.

Passt gut zum hackerspace, und vielleicht können wir ja hier und dort
helfen oder es ergibt sich eine weitere Zusammenarbeit.
