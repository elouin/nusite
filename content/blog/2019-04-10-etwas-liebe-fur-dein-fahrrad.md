+++
title = "Etwas Liebe für dein Fahrrad"
date = 2019-04-10T13:07:42Z
author = "typ_o"
path = "/blog/2019/04/10/etwas-liebe-fur-dein-fahrrad"
aliases = ["/blog/archives/428-Etwas-Liebe-fuer-dein-Fahrrad.html"]
+++
[![](/media/fd24721f23eaf34669c7507bf047a8b306030c0d.serendipityThumb.jpeg)](/media/fd24721f23eaf34669c7507bf047a8b306030c0d.jpeg)

Wir machen gemeinsam unsere Räder fit für die Saison:

- Putzen
- Flicken
- Reparieren

Werkzeug und Material ist vorhanden.

Sonntag, 14. April 15:00 Uhr vor dem
[Space](/kontakt/ "asdf").
