+++
title = "Basteln im 21. Jahrhundert"
date = 2009-12-04T17:59:38Z
author = "typ_o"
path = "/blog/2009/12/04/basteln-im-21-jahrhundert"
aliases = ["/blog/archives/58-Basteln-im-21.-Jahrhundert.html"]
+++
[Die Demokratisierung des
Produktionswissens](https://chaosradio.ccc.de/cre138.html), Podcast des
Chaosradio Express mit Tim Pritlove, Frank Rieger (CCC) und Philip
Steffan ([bausteln.de](https://bausteln.de/)).

Die technische Entwicklung erlaubt es mittlerweile sich am Bau von
Dingen zu versuchen, die vor kurzem noch unrealisierbar schienen. Eine
neue Nerdgerätebranche entsteht, die zu vergleichsweise günstigen
Preisen Geräte für den Hausgebrauch anbietet, die bislang nur in der
Fertigungsindustrie verfügbar waren.

Im Gespräch mit Tim Pritlove berichten Frank Rieger und Philip Steffan
von den technischen, kulturellen und gesellschaftlichen Entwicklungen,
die diese Bewegung unterstützen und erzählen auch von ihren eigenen
Projekten, empfehlenswerten Gerätetypen und anderen nützlichen Resourcen
und Quellen.
