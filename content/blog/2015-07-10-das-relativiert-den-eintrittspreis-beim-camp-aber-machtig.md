+++
title = "Das relativiert den Eintrittspreis beim Camp aber mächtig!"
date = 2015-07-10T14:55:45Z
author = "typ_o"
path = "/blog/2015/07/10/das-relativiert-den-eintrittspreis-beim-camp-aber-machtig"
aliases = ["/blog/archives/310-Das-relativiert-den-Eintrittspreis-beim-Camp-aber-maechtig!.html"]
+++
Dass es zu Easterheggs oder irgend welchen Camps zu den Eintrittskarten
auch ein lustiges Badge in Form einer Platine gab, die lustig blinken
oder piepen konnte ist nicht neu. Was mich aber echt umgehauen hat: Beim
CCC Camp in diesem Jahr ist das Badge [ein komplettes, funktionsfähiges
Software Defined Radio](https://rad1o.badge.events.ccc.de/) - und zwar
nicht nur ein Empfänger, sondern ein (halbduplex) Transceiver!
Wooooha!
