+++
title = "Werkzeug: Tauchsäge"
+++

Im Space haben wir eine Tauchsäge von Festool. Um genau zu sein eine [TS
55
REBQ-Plus](https://www.festool.de/produkte/saegen/tauchsaegen/576000---ts-55-rebq-plus#%C3%9Cbersicht).

Eine Anleitung gibt es hier:
[Anleitung](https://festoolcdn.azureedge.net/productmedia/Images/attachment/765f89c3-755c-11e9-80fb-005056b31774.pdf)

Für die Tauchsäge existieren zudem einige Meter an Führungsschienen.

## Verschleisteile

### Sägeblätter

Wir haben sehr gute Erfahrungen mit den [Feinzahn-Sägeblättern von
Festool
(W48)](https://www.festool.de/zubehoer/491952---160x2,2x20-w48)
gemacht.

Eine lokale Quelle für den Erwerb könnte [Eisen
Krug](https://www.openstreetmap.org/node/2603685882) sein.
Das ist allerdings noch nicht verifiziert.
