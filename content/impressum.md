+++
title = "Impressum"
+++

Angaben gemäß § 5 TMG und Verantwortlich für den Inhalt nach § 55 Abs. 2 RStV

```
flipdot e.V.
Franz-Ulrich-Straße 18
34117 Kassel
```

Kontakt

* Allgemein: `com[ät]flipdot.org` (von allen Mitgliedern einsehbar, wird archiviert)
* Vorstand: `vorstand[ät]flipdot.org` (wird archiviert)

Unsere Vereinsräume sind hier:

```
flipdot e.V.
Franz-Ulrich-Straße 18
34117 Kassel
```

Gläubiger-Identifikationsnummer: DE67ZZZ00001825855  
Registernummer: VR4781 KASSEL
