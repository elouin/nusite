+++
title = "CNC-Fräse: Material"
+++

## Alu

<http://alu-verkauf.de/>

Was du kaufen möchtest ist: AlCuMg1. Das ist
optimal zerspanbar und schmiert den Fräser nicht voll. Einziger
Nachteil: Nicht (gut) eloxierbar
