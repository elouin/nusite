+++
title = "Cryptoworkshop im Kollektivcafé Kurbad"
date = 2018-04-07T11:59:47Z
author = "wegenerd"
path = "/blog/2018/04/07/cryptoworkshop-im-kollektivcafe-kurbad"
aliases = ["/blog/archives/402-Cryptoworkshop-im-Kollektivcafe-Kurbad.html"]
+++
Im Rahmen der Vortragsreihe „[Aluhut und
gut?](https://dorn.blogsport.de/2018/02/09/aluhut-und-gut-von-internetueberwachung-zu-digitaler-selbstverteidigung/)“
haben wir am Donnerstag, den 29.03.2018, einen Workshop zum Thema
„verschlüsselte Kommunikation“ im [Kollektivcaf
Kurbad](https://www.kurbad-jungborn.de/cafe.htm) durchgeführt. Wir haben
uns sehr über das große Interesse gefreut und konnten den Teilnehmern
die Nutzung von GPG mit
[Thunderbird](https://www.mozilla.org/de/thunderbird/) und
[Enigmail](https://enigmail.net/) näher bringen. Wir haben auch über
[S/MIME](https://de.wikipedia.org/wiki/S/MIME) gesprochen, aufgrund der
begrenzten Zeit jedoch nur GPG eingerichtet. Für verschlüsselte
Kommunikation mit dem Smartphone haben wir [Signal](https://signal.org/)
empfohlen und kurz vorgestellt.

![](/media/2018-04-07_kryptocafe.serendipityThumb.jpg)  

Knapp zusammengefasst besteht die Einrichtung von GPG aus den folgenden
Schritten:

  - Thunderbird installieren
  - Mail Account einrichten
  - Enigmail aus Thunderbird heraus als Add-on installieren
  - Schlüsselpaar erstellen und sichern. **Hinweis:** Hier gab es
    während des Workshops ein kleines Durcheinander. Enigmail verwendet
    [seit
    kurzem](https://pep.foundation/blog/enigmail-2-with-pretty-easy-privacy-pep-support-by-default-for-new-users/)
    standardmäßig [p≡p](https://pep-project.org/). Dieses erledigt die
    Schlüsselgenerierung automatisch und unterscheidet sich in
    mancherlei Hinsicht von der bisherigen Vorgehensweise.
  - Mit seinem Kommunikationspartner eine Mail austauschen, um sich die
    Schlüssel zu senden und die Fingerabdrücke zu vergleichen

Eine ausführliche Anleitung findet sich Beispielsweise [im c't
Magazin.](https://www.heise.de/ct/artikel/Einfach-erklaert-E-Mail-Verschluesselung-mit-PGP-4006652.html)

Vielen Dank an der Stelle nochmal an die interessierten Teilnehmer sowie
an das [Kollektivcafé Kurbad](https://www.kurbad-jungborn.de/cafe.htm)
für die Ausrichtung der Veranstaltungsreihe und Bereitstellung der
Räumlichkeiten!

Großes Interesse bestand auch am Thema Festplattenverschlüsselung. Wie
das geht, werden wir im Rahmen unserer Veranstaltung
[HackWat](https://flipdot.org/wiki/HackWat) thematisieren. Jeder ist
herzlich eingeladen!
