+++
title = "Flipdot hat Räume!"
date = 2010-03-19T12:48:36Z
author = "typ_o"
path = "/blog/2010/03/19/flipdot-hat-raume"
aliases = ["/blog/archives/79-Flipdot-hat-Raeume!.html"]
+++
![](/media/Gueterabefrtigung_EG_Ausschnitt.serendipityThumb.jpg)Vergangenen
Dienstag haben wir zusammen beschlossen, die [Räume in der "Zentralen
Netzleitstelle"](https://flipdot.org/wiki/index.php?title=Raumsuche/G%C3%BCterabfertigung)
am Kasseler Hauptbahnhof zu mieten. Wir werden für's Erste ca 50 m² im
Erdgeschoß und dazu einen guten, trockenen Kellerraum z.B. für Maschinen
haben. Ab dem ersten April können wir rein, und uns die Räume
herrichten. Die Netzwerk-Infrastruktur ist noch vorhanden, wir müssen
lediglich einen Server in den Schrank im 1. OG schrauben und die
Patchkabel einstecken.  
Wenn alles klappt, und wir auch rechtzeitig den Schlüssel haben, wollen
wir uns gleich am 1.4. um 19:00 dort treffen.
