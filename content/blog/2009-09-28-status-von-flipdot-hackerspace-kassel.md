+++
title = "Status von flipdot - hackerspace kassel"
date = 2009-09-28T12:37:00Z
author = "typ_o"
path = "/blog/2009/09/28/status-von-flipdot-hackerspace-kassel"
aliases = ["/blog/archives/9-Status-von-flipdot-hackerspace-kassel.html"]
+++
flipdot ist [frühes
beta](https://de.wikipedia.org/wiki/Entwicklungsstadium_%28Software%29).
Es gibt schon einen vorläufigen Namen, ein vorläufiges Logo (Danke Olaf
für die Überarbeitung), einen vorläufigen Flyer, und gerade schreiben
[Olaf](https://olafval.de/aktuel/aktuel.htm),
[Rick](https://e-bildwerke.de/) und [ich](https://infragelb.de/tinker/) an
einem vorläufigen Konzeptpapier, das Menschen von der Stadt und dem
womöglichen Vermieter vorgelegt werden soll für vorläufige Gedanken
hinsichtlich Fördermöglichkeiten. Und was ist denn schon fest? Kassel
wird in Kürze einen Hackerspace haben! (Vorgehensweise frei nach "[The
Cult of Done
Manifesto](https://www.brepettis.com/blog/2009/3/3/the-cult-of-done-manifesto.html)")
