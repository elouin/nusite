Blog: Kaputte Links auf externen Seiten

## Artikel von 2020

https://soundcloud.com/user-976695732-112743983/s2e1kulturnapf-interview-flipdot
https://soundcloud.com/user-976695732-112743983
https://www.bffk.de/aktuelles/pflegekammerumfrage-in-niedersachsen-gestoppt-vollstaendiger-neustart-wahrscheinlich.html
https://s.flx.ai/2020/cellular-automata/

## Artikel von 2019

https://github.com/flipdot/drinks-touch/blob/develop/drinks_touch/notifications/notification.py

## Artikel von 2018

https://www.br.de/nachrichten/das-neue-polizeiaufgabengesetz-kurz-erklaert-100.html
https://www1.wdr.de/nachrichten/ruhrgebiet/cybercrime-razzia-dortmund-nordstadt-100.html
https://datenfresser.info/?p=191
https://www.ies.uni-kassel.de/p/i%C2%B2othack/
https://www.ies.uni-kassel.de/p/i²othack/
https://www.augsburger-allgemeine.de/augsburg/Als-der-Staatsschutz-ins-Augsburger-Open-Lab-kam-id51549741.html?utm_campaign=Echobox&utm_medium=augsburg&utm_source=Twitter#Echobox=1530695341: Anchor `#Echobox=1530695341` not found on page

## Artikel von 2017

https://www.hessen.de/pressearchiv/pressemitteilung/sicherheitsluecken-schaden-betrieben-0
https://flipdot.org/wiki/Was%20Sie%20schon%20immer%20%25C3%25BCber%20Technik%20wissen%20wollten...
https://wirtschaft.hessen.de/sites/default/files/media/hmwvl/leitfaden_vertraulichkeitsschutz_durch_verschluesselung.pdf

## Ältere Artikel

https://www.geekgiftshop.net/content-categories/cat-403_404/illuminated_shirts.html
https://www.watterott.com/de/blog/Wanderkiste
https://www.watterott.com/de/RF-Link-Sender-434MHz
https://www.dl2bcm.de/down/Koaxant.pdf
https://market.android.com/details?id=com.kvndev.android.two
https://www.merianverlag.ch/buecher/detail.cfm?ObjectID=5B23A74F-1422-0CEF-B45B1F9DC99E47EE
https://shop.npr.org/catalog/Geek_Bouteek-34-1.html
https://www.buglabs.net/products
https://www.kassel-essentrinken.de/kunden/gastro/KS/st/sanmarino/vk.html
https://addons.mozilla.org/de/firefox/addon/16
https://infragelb.de/tinker/
https://infragelb.de/flipdot-power/
https://sigint.ccc.de/sigint/2009/Fahrplan/events/3197.en.html
https://datasheets.maximintegrated.com/en/ds/MAX4711-MAX4713.pdf
https://openlab-augsburg.de/
https://www.jahtari.org/artists/disrupt.htm
https://clockworker.de/cw/2013/12/03/der-thermoelektrische-aetherwellen-empfaenger/
https://www.teo.lt/node/1066
https://www.teo.lt/gallery/flash/Telekomun.swf
https://image.channeladvisor.de/128021/8874d09f54c8f9aca8eba758928aed09.jpg
https://www.simonmason.karoo.net/page30.html
https://repaircafe-ks.de/
https://www.kvndev.com/android/two
https://cacher.dozuki.net/static/images/manifesto/ifixit_manifesto_1650x2550.jpg
https://cacher.dozuki.net/static/images/manifesto/ifixit_manifesto_1650x2550.jpg
https://news.jeelabs.org/projects/
https://flipdot.spreadshirt.de/
https://flipdot.wegenerd.de
https://www.cl-bergmann.de/
https://www.dx.com/p/lm2596-digital-display-adjustable-step-down-voltage-regulator-module-dark-blue-360654
https://www.styleon.de/index.html?cmd=setshoprubrik&rubrik=283
https://www.expedition-zukunft.org/alias/Kassel/985327
https://labs.nortd.com/lasersaur/
https://www.poc21.cc/
https://blog.makezine.com/archive/make_podcast/
https://www.mathe-shirts.de/mathemagiker/gauss/
https://www.raspberrypi.org/gert-vga-adapter/
https://www.raspberrypi.org/raspberry-pi-model-a-plus-on-sale/
https://gist.github.com/swege/ee89056fc857a3f37e05
https://flipdot.org/wiki/images/6/64/2010_10_22_Bausteln2.pdf
https://labs.echonest.com/Uploader/index.html?trid=TRIGZGH13DF800F8BD
https://hessenschau.de/gesellschaft/1-hr-hackathon,hackathon-102.html
https://www.paulinevandongen.nl/eng/2012/flip-dot-dress-the-process/
https://www.flipdots.com/advantages.html
https://www.swldxer.co.uk/dlf.wma
https://nutcom.hu/?page_id=108
https://www.fingers-welt.de/jubilaeum/treffen2009.htm
https://www.atelierk10.de/?programm&WS_201011
https://www.conrad.de/ce/
https://events.ccc.de/congress/2013/wiki/images/0/06/30c3-map_v0.2-beta2.png
