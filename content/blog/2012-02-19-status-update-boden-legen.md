+++
title = "Status Update: Boden legen"
date = 2012-02-19T17:00:07Z
author = "typ_o"
path = "/blog/2012/02/19/status-update-boden-legen"
aliases = ["/blog/archives/161-Status-Update-Boden-legen.html"]
+++
Die Räume sind jetzt bezugsfertig, ein paar Streicharbeiten fehlen noch.
Kommende Tasks: Küchenecke, Licht, und alles Arbeitsmaterial einräumen.
Die Aktion hat Spaß gemacht!

![](/media/hinten_boden.serendipityThumb.jpg)

![](/media/boden_transport.serendipityThumb.jpg)

![](/media/boden_draussem.serendipityThumb.jpg)

![](/media/vorne_boden.serendipityThumb.jpg)
