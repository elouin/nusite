+++
title = "Pegelwandler mit automatischer (!) Richtungserkennung"
date = 2013-04-16T05:38:18Z
author = "typ_o"
path = "/blog/2013/04/16/pegelwandler-mit-automatischer-richtungserkennung"
aliases = ["/blog/archives/196-Pegelwandler-mit-automatischer-!-Richtungserkennung.html"]
+++
[TXB0108](https://www.ti.com/product/txb0108) ist ein schlauer Baustein
von Texas Instruments, der z.B. die 3,3 V Portspannungen vom Raspberry
Pi nach 5 V wandelt. In beide Richtungen. Und erkennt dabei automatisch,
ob ein Port Eingang oder Ausgang ist. Fantatstique!
