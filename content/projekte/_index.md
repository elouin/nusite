+++
title = "Projekte"
template = "projekte.html"
sort_by = "title"
+++

Da Dokumentation nicht wirklich unsere Stärke ist, hängt diese Liste der
Realität hinterher. Derzeit fehlen Dokus zu...

- Alle Energiesparprojekte unter dem Label "SEK: E-Traktiv"
  - messtechnische Untersuchung von LED Lampen
  - Gegenstrom-Wärmetauscher
  - Funksteckdosen
- 3D-Drucker
  - Umbautens
  - Filamenttrockner mit Molekularsieb
  - Doku der Druckprojekte
  - Testdrucke / Lessons Learned
  - Kleiderständer
  - Gehäuse
- Statistiken und grafische Darstellungen [in unserer
    Grafana-Instanz](https://stats.flipdot.org/dashboard/db/users)
- Quadcopter
- Internet of Dings - MQTT Netzwerk
- Aussenbeblinkung:
  - Feuerwerkssimulation auf dem Dach,
  - fd-Sign am Eingang
- Innenbeblinkung:
  - Member FiWi Stats mit Physik - Engine
  - Dual Uhr
  - 8x8 low res Projektor
  - MQTT Wabenlampe
  - Getränkeregalblinkenlights
- CNC Fräse
  - Erweiterungen / Ergänzungen
    - Stifthalter
    - Meßuhrhalter
    - Zyklon-Staubabscheider
  - Doku der Fräsprojekte
- Ausrüstung und Verfahren chemische Leiterplattenfertigung
- Ausrüstung und Verfahren CNC Leiterplattenfertigung
- Doku Mikroskope (3D und 2D mit Monitoranschluss)
- Bargeldloses Getränke Buchungssystem
- GSM Türschloss unten
- SSH Türschloss oben

Bitte dokumentiere dein Projekt oder auch die von anderen, wenn Sie nichts dagegen haben.
