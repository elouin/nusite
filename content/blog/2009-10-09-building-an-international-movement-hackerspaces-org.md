+++
title = "Building an international movement: hackerspaces.org"
date = 2009-10-09T01:00:00Z
author = "typ_o"
path = "/blog/2009/10/09/building-an-international-movement-hackerspaces-org"
aliases = ["/blog/archives/23-Building-an-international-movement-hackerspaces.org.html"]
+++
Präsentation auf dem 25C3 27.12.2008, [Nick Farr, Jens Ohlig, Bre, Jacob
Appelbaum, Enki, Philippe
Langlois](https://chaosradio.ccc.de/25c3_m4v_2806.html).
