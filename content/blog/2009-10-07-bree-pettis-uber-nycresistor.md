+++
title = "Bree Pettis über NYCResistor"
date = 2009-10-07T01:00:00Z
author = "typ_o"
path = "/blog/2009/10/07/bree-pettis-uber-nycresistor"
aliases = ["/blog/archives/25-Bree-Pettis-ueber-NYCResistor.html"]
+++
Bre Pettis über NYCResistor, ein Hacker-Kollektiv in Brooklyn. NYCR
nimmt den Kollektivgedanken auf unamerikanische Weise sehr ernst, Bre
sagte in irgendeinem Podcast mal, jeder im NYCR müßte jedem anderen dort
seinen Wohnungsschlüssel für vier Wochen leihen können.

[Bre Pettis](https://www.brepettis.com/), Gründer von
[Makerbot](https://www.makerbot.com/) und
[NYCResistor](https://www.nycresistor.com/), Mitgründer von
[Thingiverse](https://www.thingiverse.com/), "a web-based community to
promote creating and free sharing of rapid prototype designs", beteiligt
an der medialen Gestaltung von [Etsy.com](https://www.etsy.com/), hat für
das [Make: Magazine](https://makezine.com/) den [Weekend Projects
podcast](https://blog.makezine.com/archive/make_podcast/) produziert.

[Interview](https://chaosradio.ccc.de/cri014.html) mit Bre Pettis im
Chaosradio, Vortrag von Bre auf dem 25C3, [Rapid Prototype Your
Life](https://chaosradio.ccc.de/25c3_m4v_3015.html).
