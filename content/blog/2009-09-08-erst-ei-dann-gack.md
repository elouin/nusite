+++
title = "Erst Ei, dann Gack!"
date = 2009-09-08T09:47:00Z
author = "typ_o"
path = "/blog/2009/09/08/erst-ei-dann-gack"
aliases = ["/blog/archives/2-Erst-Ei,-dann-Gack!.html"]
+++
**Problem** You have a chicken-and-egg-problem: What should come ﬁrst?
Infrastructure or projects?

**Implementation** Make everything infrastructure-driven. Rooms, power,
servers, connectivity, and other facilities come ﬁrst. Once you have
that, people will come up with the most amazing projects you didn’t
think about in the ﬁrst place. (Q: [Design Patterns for
Hackerspaces](https://wiki.hackerspaces.org/Design_Patterns))
