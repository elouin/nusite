+++
title = "Podcast: Hacker- und Makerspaces"
date = 2016-05-06T10:30:38Z
author = "typ_o"
path = "/blog/2016/05/06/podcast-hacker-und-makerspaces"
aliases = ["/blog/archives/342-Podcast-Hacker-und-Makerspaces.html"]
+++
Seit geraumer Zeit existieren in fast allen größeren Städten sogenannte
Hacker- oder Makerspaces. Gleichzeitig wird das sogenannte Coworking
immer beliebter. Nach welchem Prinzip funktionieren diese Orte
kollaborativen Arbeitens?

Was hat sich über die Jahre verändert? Wo liegen die Unterschiede
zwischen Hackerspace, Makerspace und Coworking und wie unterscheiden
sich die Einrichtungen in ihren Ideen und Philosophien? Und was ist zu
beachten, wenn man selber einen solchen Space gründen möchte?

Hier gibts den Podcast:
[Chaosradio 222](https://chaosradio.ccc.de/cr222.html).
