+++
title = "Film meets Hacker - Dokfilmfest in Kassel"
date = 2010-11-10T07:55:12Z
author = "typ_o"
path = "/blog/2010/11/10/film-meets-hacker-dokfilmfest-in-kassel"
aliases = ["/blog/archives/120-Film-meets-Hacker-Dokfilmfest-in-Kassel.html"]
+++
Das Kasseler Dokumentarfilm- und Videofest zeigt in Kooperation mit
flipdot - hackerspace kassel drei Filme zu den Themen  
**Schöne, neue Welt der Roboter** ([Plug &
Pray](https://www.filmladen.de/dokfest/programm-2010/programm/freitag-12-11-2010/view/plug-and-pray)
- Freitag 12.11.2010 / 17:00 h / Gloria Kino)  
**Was ist ein Hacker?**
([Hacker](https://www.filmladen.de/dokfest/programm-2010/programm/freitag-12-11-2010/view/hacker)
- Freitag 12.11.2010 / 23:30 h / Filmladen)  
**Orientierung im Neben-, Über- und Durcheinander der analogen und
digitalen Schnittstellen** ([Ana +
Digi](https://www.filmladen.de/dokfest/programm-2010/programm/samstag-13-11-2010/view/ana-digi)
- Samstag 13.11.2010 / 17:30 h / BALi Kinos)
