+++
title = "Hörspiel: Daemon von Daniel Suarez"
date = 2013-04-19T04:07:02Z
author = "typ_o"
path = "/blog/2013/04/19/horspiel-daemon-von-daniel-suarez"
aliases = ["/blog/archives/199-Hoerspiel-Daemon-von-Daniel-Suarez.html"]
+++
Daniel Suarez, geboren 1964, war selbst Systemberater und
Software-Entwickler, bevor er anfing, seine Techno-Thriller zu
schreiben. Seinen Debütroman "DAEMON" gibt es gerade als
[Hörspiel](https://www.einslive.de/sendungen/plan_b/krimi/2013/daemon/130408_inhalt.jsp)
bei den öffentlich rechtlichen. Schnell runterladen, bevor
depubliziert!
