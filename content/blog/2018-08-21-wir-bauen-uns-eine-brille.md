+++
title = "Wir bauen uns eine Brille"
date = 2018-08-21T22:22:55Z
author = "Baustel"
path = "/blog/2018/08/21/wir-bauen-uns-eine-brille"
aliases = ["/blog/archives/413-Wir-bauen-uns-eine-Brille.html"]
+++
[![](/media/IMG_20180821_230946.serendipityThumb.jpg)](/media/IMG_20180821_230946.jpg)

Was macht man, wenn einem seine alte Brille nicht mehr gefällt, man aber
Holz und eine CNC-Fräse zur Verfügung hat? Genau, man fräst sich einfach
eine Neue. Work in progress, zur Zeit sind wir an den Bügeln dran:

[![](/media/IMG_20180821_230653.serendipityThumb.jpg)](/media/IMG_20180821_230653.jpg)
