+++
title = "Watts-up-Pi misst Watts"
date = 2014-12-11T19:53:04Z
author = "typ_o"
path = "/blog/2014/12/11/watts-up-pi-misst-watts"
aliases = ["/blog/archives/265-Watts-up-Pi-misst-Watts.html"]
+++
Der [Pi ist schon lange
montiert](https://flipdot.org/blog/archives/254-Watts-up.html) (Danke
Burkard!), jetzt loggt er auch die Leistung:

[![](/media/watts-up-pi-power.serendipityThumb.png)](/media/watts-up-pi-power.png)
