+++
title = "Neue Räume - fast schon wieder zu klein!"
date = 2016-05-18T10:15:27Z
author = "typ_o"
path = "/blog/2016/05/18/neue-raume-fast-schon-wieder-zu-klein"
aliases = ["/blog/archives/343-Neue-Raeume-fast-schon-wieder-zu-klein!.html"]
+++
Praktisch jeden Tag sind jetzt Mitglieder in den neuen Räumen zugange.
Praktisch jeden Tag werden neue Ideen bebrütet und neue Installationen
fertig. Gestern, am offenen Dienstag, waren mehr neue Besucher da denn
je. Der Umzug hat sich gelohnt, und alle (alle? alle!) sind von den
neuen Räumen begeistert.

[![](/media/000_0.serendipityThumb.jpg)](/media/000_0.jpg)

[![](/media/000_1.serendipityThumb.jpg)](/media/000_1.jpg)

[![](/media/001_0.serendipityThumb.jpg)](/media/001_0.jpg)

[![](/media/002_0.serendipityThumb.jpg)](/media/002_0.jpg)

[![](/media/002_1.serendipityThumb.jpg)](/media/002_1.jpg)

[![](/media/004_0.serendipityThumb.jpg)](/media/004_0.jpg)

[![](/media/005_0.serendipityThumb.jpg)](/media/005_0.jpg)

[![](/media/008_0.serendipityThumb.jpg)](/media/008_0.jpg)

[![](/media/008_1.serendipityThumb.jpg)](/media/008_1.jpg)

[![](/media/009_0.serendipityThumb.jpg)](/media/009_0.jpg)

[![](/media/010_0.serendipityThumb.jpg)](/media/010_0.jpg)

[![](/media/011_0.serendipityThumb.jpg)](/media/011_0.jpg)

[![](/media/012_0.serendipityThumb.jpg)](/media/012_0.jpg)

[![](/media/013_0.serendipityThumb.jpg)](/media/013_0.jpg)

[![](/media/014_0.serendipityThumb.jpg)](/media/014_0.jpg)

[![](/media/015_0.serendipityThumb.jpg)](/media/015_0.jpg)

[![](/media/017_0.serendipityThumb.jpg)](/media/017_0.jpg)

[![](/media/018_0.serendipityThumb.jpg)](/media/018_0.jpg)

[![](/media/019_0.serendipityThumb.jpg)](/media/019_0.jpg)
