+++
title = "facebook hat kein \"Datenleck\" - das ist genau so vorgesehen"
date = 2018-03-23T12:45:00Z
author = "typ_o"
path = "/blog/2018/03/23/facebook-hat-kein-datenleck-das-ist-genau-so-vorgesehen"
aliases = ["/blog/archives/399-facebook-hat-kein-Datenleck-das-ist-genau-so-vorgesehen.html"]
+++
Facebook hatte im engeren Sinne kein Datenleck. Stattdessen sind die
Daten auf einem von der Plattform regulär vorgesehenen Weg an das
Unternehmen GSR geflossen, das sie an Cambridge Analytica weiterleitete.
GSR erstellte eine App, mit der Facebook-Nutzer einen
Persönlichkeitstest machen konnten. Die Schnüffel-App griff dann
planmäßig über eine Programmierschnittstelle von Facebook nicht nur
auf die Daten derjenigen, die die App benutzten, sondern auch auf die
ihrer Freunde zu.

Facebook hat also das System, in dem die Daten von Millionen Menschen
ohne ihre Einwilligung bei anderen Firmen landen, selbst geschaffen.
Dementsprechend [schloss Mark Zuckerberg \[..\] auch nicht
aus](https://www.facebook.com/zuck/posts/10104712037900071), dass andere
Unternehmen mit ähnlichen Apps auch viele Daten gesammelt haben
könnten.

[Quelle](https://netzpolitik.org/2018/facebook-endlich-die-bunten-apps-rausschmeissen-eine-anleitung/).
Dort ist auch beschrieben, wie man die Schnüffel-App von facebook
entfernt, und seine Datenschutzeinstellung schärfer einstellt.  

> Wir empfehlen jedoch: facebook komplett verlassen und vorher alles
> löschen, soweit möglich. (Natürlich keine Garantie, dass die Daten
> *wirklich* weg sind).  
>   
> [Löschanleitung für das komplette facebook
> Konto](https://datenfresser.info/?p=191)
