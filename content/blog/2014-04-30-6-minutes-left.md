+++
title = "6 minutes left"
date = 2014-04-30T16:41:26Z
author = "typ_o"
path = "/blog/2014/04/30/6-minutes-left"
aliases = ["/blog/archives/237-6-minutes-left.html"]
+++
[![](/media/timetables.serendipityThumb.jpg)](/media/timetables.jpg)

Work in progress: Unser kleiner
[Touch-Pi](https://flipdot.org/blog/archives/235-Laser!.html)
hat seine ersten Teilaufgaben in der Space-Steuerung schon bekommen,
hier die von der ÖPNV-Website runtergeparsten Abfahrtszeiten der
nächsten Bahnen (Die [kvg](https://kvg.de/) bietet keinerlei vernünftige
API an, denn dann könnten die Kunden ja gucken, wann sie die nächste
Transportdienstleistung kaufen können - bewahre! Die haben da irgendein
proprietäres System \[geleast?\], wo sie so nette Sachen wie im Bild -
sowas hängt dann in offizieller Version im Auebad - jeweils einzeln
teuer latzen müssen)

Ausserdem muss ich bei der Gelegenheit mal über deren grauenhafte Site
ranten, wo die Fahrplansuche in einem Winz-scroll-dich-tot-Frame läuft!
Das ist so 1995!
