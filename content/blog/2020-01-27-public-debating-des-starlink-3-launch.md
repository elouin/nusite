+++
title = "Public DEBATING des Starlink-3 Launch"
date = 2020-01-27T14:00:00Z
author = "Baustel"
path = "/blog/2020/01/27/public-debating-des-starlink-3-launch"
aliases = ["/blog/archives/434-Public-DEBATING-des-Starlink-3-Launch.html"]
+++
![Hispasat 30W-6 Mission by SpaceX on
flickr](https://live.staticflickr.com/4753/25790223907_9ea3e63180_w.jpg)  
**Weltraum Nerds (nichtmehr) aufgepasst!**  
Der Start wurde nochmal verschoben. Über deinen Besuch unseres Offenen
Abends freuen wir uns trotzdem!  

> Standing down today due to strong upper level winds. Next launch
> opportunity is tomorrow at 9:28 a.m. EST, or 14:28 UTC.
> 
> — SpaceX (@SpaceX)
> [January 27, 2020](https://twitter.com/SpaceX/status/1221799899848531969?ref_src=twsrc%5Etfw)

<!-- more -->

Nachdem der Start zum zweiten Mal verschoben werden musste, wird am
Dienstag den 28. Januar um 15:49 Uhr unserer Zeit die nächste Batch
Starlink Satelliten ab Cape Caneveral gestartet werden. Da sich das
hevorragend mit unserem Offenen Abend trifft, möchten wir alle Kreaturen
einladen gemeinsam mit uns den Livestream zu verfolgen.  
Falls ihr Lust und Zeit habt komm doch vorbei und wir schauen uns an wie
eine Falcon 9 Trägerrakete startet und landet. Anschliessend wird dann
laut Flugplan eine Stunde später die Payload in circa 300 km Höhe
ausgeworfen.

Falls das Wetter mitspielt, können wir im Anschluss gegen 18 Uhr den 60
Satelliten der letzten Batch, Starlink-2, zuschauen wie sie sich mit
einem leuchtendem Hall-Effekt-Antrieb selbstständig in die Arbeitshöhe
von etwa 500 km drücken und dabei etwa fünf Minuten über unsere Köpfe
ziehen.

Ausserdem erwarten dich wie jeden Dienstag interresante und
interresierte Menschen, sowie kalte Getränke. Wir freuen uns auf dich!
