+++
title = "Ausprobieren mit Pygame, RasPi und Pollin LCD"
date = 2014-11-10T15:01:44Z
author = "typ_o"
path = "/blog/2014/11/10/ausprobieren-mit-pygame-raspi-und-pollin-lcd"
aliases = ["/blog/archives/261-Ausprobieren-mit-Pygame,-RasPi-und-Pollin-LCD.html"]
+++
Hier so ein paar Vorübungen zu einer GUI auf dem Pi.

[![](/media/pygame_mockup.serendipityThumb.jpg)](/media/pygame_mockup.jpg)

Man weiss ja immer nicht so richtig, welche Schriften und Farben man
nehmen soll. Weil die Pygame-Lib Truetype unterstützt, ist z.B.
[dafont.com](https://www.dafont.com/) eine gute Adresse,
[diese](https://www.dafont.com/white-rabbit.font) Schrift geht gut als
Monospaced.

Bei der Suche nach angenehmen Farbpaletten hilft [Adobe Color
CC](https://color.adobe.com/de/explore/most-popular/?time=all).
