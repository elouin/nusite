+++
title = "IoT: Netzwerk"
+++

- Subnetz: 192.168.42.0/24
- Gateway: 192.168.42.254
- DHCP-Range: .100 - 250
- SSID: security-by-iod
- Key: \<siehe Forum\>

<!--
## Statische IP-Adressen

muss \< .100 \|\|IP \|\|ESP-ID = MAC\|\|Geraet \|\|Ort \|\|Bemerkung
\|\|Verantwortlicher \|\| \|\|192.168.42.1 \|\| \|\|iod-cellar-lede
\|\|Raum hinter oranger Tür \|\| \|\|feliks \|\| \|\|192.168.42.10 \|\|
\|\|sensor/door/test \|\|Lounge neben Tür \|\| \|\|typ_o \|\|
\|\|192.168.42.11 \|\| \|\|sensor/button/test \|\|Lounge neben Tür \|\|
\|\|typ_o \|\| \|\|192.168.42.12\|\|2c:3a:e8:27:3b:a8\|\| \|\| \|\|
\|\|typ_o\|\| \|\|192.168.42.13\|\|60:01:94:49:de:4e\|\| \|\| \|\|
\|\|typ_o\|\| \|\|192.168.42.14\|\|60:01:94:49:e4:98\|\| \|\| \|\|
\|\|typ_o\|\| \|\|192.168.42.15\|\|2c:3a:e8:27:3c:1f\|\| \|\| \|\|
\|\|typ_o\|\| \|\|192.168.42.16\|\|2c:3a:e8:27:44:01\|\| \|\| \|\|
\|\|typ_o\|\| \|\|192.168.42.17\|\|a0:20:a6:10:6e:17\|\| \|\| \|\|
\|\|typ_o\|\| \|\|192.168.42.18\|\|60:01:94:49:e1:4b\|\| \|\| \|\|
\|\|typ_o\|\| \|\|192.168.42.19\|\|a0:20:a6:10:6e:18\|\| \|\| \|\|
\|\|typ_o\|\| \|\|192.168.42.20\|\|60:01:94:49:d9:40\|\| \|\| \|\|
\|\|typ_o\|\| \|\|192.168.42.21\|\|a0:20:a6:10:6e:03\|\| \|\| \|\|
\|\|typ_o\|\| \|\|192.168.42.22\|\| \|\| Sonoff \|\| M-Werkstatt \|\|
\|\|malled \|\| \|\| \|\| \|\| \|\| \|\| \|\| \|\| \|\| \|\| \|\| \|\|
\|\| \|\| \|\|
-->
