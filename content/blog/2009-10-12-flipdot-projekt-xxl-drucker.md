+++
title = "flipdot Projekt: XXL-Drucker"
date = 2009-10-12T11:31:00Z
author = "typ_o"
path = "/blog/2009/10/12/flipdot-projekt-xxl-drucker"
aliases = ["/blog/archives/41-flipdot-Projekt-XXL-Drucker.html"]
+++
Mein Besuch bei flipdot Member Arne brachte uns neben angeregtem
Geplauder gleich dazu, einen Versuch für die Spritzdüsen meines
geplanten XXL-Druckers zu frickeln.

![](/media/printer03.jpg)

Die Farbe soll von einem Kapilarröhrchen angesaugt und durch die von
links zugeführte Druckluft zerstäubt werden. Flugs ein Stück von diesem
Röhrchen (aus einem alten Kühlschrank) in ein dickeres Kupferrohr
gelötet:

![](/media/printer02.jpg)

Innen drin kann man noch das Ende des feinen Röhrchens erkennen:

![](/media/printer01.jpg)

[![](/media/printer04.serendipityThumb.jpg)](/media/printer04.jpg)

Gesamtplan

Erste Versuche mit Wasser verliefen ermutigend. Die Farbe soll Kreide
mit Wasser gemischt sein (abwaschbar), ggf. muß dabei noch ein
Magnetrührer eingebaut sein, damit sich der Matsch nicht absetzt.  
Die Druckluft wird später aus einem Feuerlöscher kommen, der (bis auf
weiteres) an der Tanke befüllt wird. An eine Preßluftflasche wie für
Taucher mit \>\> 100 Bar traue ich mich nicht dran.

Ein Satz Festo-Elektroventile sollen dann acht dieser Düsen ansteuern.
Dann noch etwas Controller-FooBar, und fertig ist der Druckkopf.

![](/media/alles.jpg)

Buchstabenhöhe soll ca. 90 cm sein, gedruckt wird auf
Zeitungs-Auslaufrollen oder auf die Straße.

\[Helmut\]
