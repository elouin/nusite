+++
title = "Space Reboot"
date = 2021-11-07T23:55:00Z
author = "jplatte"
path = "/blog/2021/11/07/space-reboot"
+++

An alle, die wegen Corona lange einen Besuch verschoben haben: Unser offener
Dienstag findet *mit 2G-Regelung* wieder statt!

Ab sofort könnt ihr wieder jeden Dienstag ab 19 Uhr vorbei kommen, mit der
Voraussetzung, dass ihr geimpft oder genesen seid und einen entsprechenden
Nachweis dabei habt.

Wer uns zum ersten Mal besuchen möchte, kann unter [Kontakt](/kontakt/)
nachlesen, wie man uns findet.
