+++
title = "Wen erreicht man, wenn man zu einem „Hackathon” einlädt?"
date = 2016-01-28T08:10:05Z
author = "typ_o"
path = "/blog/2016/01/28/wen-erreicht-man-wenn-man-zu-einem-hackathon-einladt"
aliases = ["/blog/archives/328-Wen-erreicht-man,-wenn-man-zu-einem-Hackathon-einlaedt.html"]
+++
Der Veranstaltungsname „Hackathon”, „Codefest” oder „Geekend” scheint
das selbe zu bezeichnen, spricht aber unterschiedliche Gruppen an. [Ein
schöner Text](https://merton-magazin.de/das-hackathon-problem) der
geschätzten [Kathrin
Passig](https://de.wikipedia.org/wiki/Kathrin_Passig) über die Wirkung
von Bezeichnern von Studiengängen und Veranstaltungen.
