+++
title = "FAQ"
+++

*flipdot hackspace kassel - alles was du über uns wissen musst*

## Was ist ein Hackspace überhaupt?

Hier treffen sich Menschen, die an Wissenschaft, Technologie oder Kunst, (Netz-)Politik,
Do-It-Yourself, und vielen anderen Themen interessiert sind. Wir verstehen uns als Ort, an dem die
Zwänge von Schule, Uni oder Betrieb nicht gelten. An diesen Orten soll schnell etwas rauskommen,
muss billig sein, nützlich. All das ist uns egal, wir haben Spaß am Gerät. Hier sind wir frei,
Dinge zu tun, „because we can“!

Wir treffen uns hier, tauschen uns aus und arbeiten an gemeinsamen oder eigenen Projekten. Wir
lernen sehr gern Neues! Stell dir einen Hackspace als eine Art offene Werkstatt vor, in der du
vieles vorfindest, das dir bei der Arbeit an einem Projekt hilft: Werkzeug, Maschinen, Wissen und
nette Leute, die dir dieses auch weitergeben. flipdot ist eine Art anarchistische Volkshochschule.

## Was wollt ihr damit erreichen?

Der Verein schafft Raum und Infrastruktur für seine Mitglieder, setzt öffentliche Treffen und
Informationsveranstaltungen um und fördert den schöpferisch-kritischen Umgang mit Technik. Wir
haben 2009 mit dem Hackspace flipdot einen Raum geschaffen, in dem sich Jung und Alt frei mit
Technik, Wissenschaft und Kunst beschäftigen können. Wir bieten einen Ort, um gemeinsam Neues zu
lernen, sich gegenseitig helfen zu können, und vor allem um Spaß am Gerät und am kreativen Basteln
zu haben. Wir diskutieren auch gern und viel über Technik, Gesellschaft und Wissenschaft, stellen
Fragen zu Sinn und Zweck, zu Sicherheit und Gefahren.

## Ich glaube nicht, dass ich ein "Hacker" bin. Kann ich trotzdem kommen?

Aber klar! Bei uns ist jeder willkommen, unabhängig von jedem Attribut. Es ist auch egal, ob du
dich als Hacker, Maker, Bastler, Neugieriger, Forscher oder Künstler bezeichnest. Sei offen,
interessiert und freundlich, und du wirst ebenso empfangen.

## Aber ich kenne mich mit [Computern | Elektronik | beliebigem Thema] überhaupt nicht aus!

Aber du bist neugierig? Dann bist du bei uns richtig. Und ein Spezialgebiet hast du bestimmt, in dem du anderen weiter helfen kannst. Wir haben Mitglieder die extrem gut konstruieren können oder Bier brauen, Programmieren, Kochen, Löten, Messer schleifen, Fehler suchen oder was auch immer! Wenn du etwas kannst, umso merkwürdiger, desto besser!

## Hackspace? Macht ihr dort etwas Illegales?

Nein! Wir halten uns an geltendes Recht und machen höchstens auf die Gefahren und Risiken von neuen Technologien aufmerksam. Wir verwenden den Begriff ‘Hacken’ in seiner ursprünglichen Definition. Gemeint ist damit nicht etwa kriminelles Eindringen in fremde Computernetze, sondern die Suche nach interessanten, unorthodoxen Lösungen für Probleme.

## Ok, gut, aber was macht ihr denn nun in eurem Hackspace?

Na, hier ein paar Beispiele:
[Fahrradverleihsysteme analysieren](https://flipdot.org/blog/archives/373-Track-a-Bike.html),
[zusammen kochen und essen](https://flipdot.org/blog/archives/364-Food-Pr0n.html),
[mit parametrischen 3D-Programmen Getränkeregale entwerfen und auf der CNC Fräse herstellen](https://flipdot.org/blog/archives/355-Eazy-math-Getraenkestorage.html), Prototypen für Kommunikationstunnel bauen,
[Hardware reparieren](https://flipdot.org/blog/archives/367-Apple-iPad1-3G-Akkutausch-Reparatur-mit-vorhandenem-Material.html),
ferngesteuerte [Türöffner](https://flipdot.org/blog/archives/351-Jeder-Space-hat-ein-Schloss.html) bauen,
[Messer schleifen lernen](https://flipdot.org/blog/archives/371-Workshop-Messer-schleifen.html),
verstehen wie schwer Kommunikation sicher gemacht werden kann,
[Antennen bauen](https://flipdot.org/blog/archives/366-Bau-einer-kolinear-Antenne-fuer-ADS-B-Empfang.html),
[zeitservergesteuerte Uhren aus Röntgenfilmbetrachter entwerfen](https://flipdot.org/blog/archives/359-Dualuhr-Update-mit-NTP-Server-Zeitserver-Abfrage.html),
[unterbrechungsfreie Stromversorgungen](https://flipdot.org/blog/archives/348-USV-fuer-raspberry-Pi-11.-Juni-2016,-2000.html) löten,
[Tetris](https://flipdot.org/blog/archives/335-Borg-Tetris.html)
mit einem Kamerasucher und einer Kransteuerung spielen, bei CCC-Veranstaltungen
[mitwirken](https://flipdot.org/blog/archives/321-flipdot-village-auf-dem-CCCAMP15-wir-sind-hier.html),
[Lampen](https://flipdot.org/blog/archives/309-Lampe.html)
bauen,
[in Mikrofone reden](https://flipdot.org/blog/archives/357-Sendung-im-Freien-Radio-Kassel.html),
Visualisierungen all unserer Daten aus dem Space programmieren, Smart Meter verstehen, Touch
Displays selber ansteuern, Besuchern mit merkwürdigen Kunstprojekten helfen, aus Mikrowellen
Schweißgeräte bauen, Hochspannungs-Plasmalautsprecher zur Funktion bringen, aus Aktivkohle und
Alufolie Batterien bauen, [brauen](https://flipdot.org/blog/archives/243-MashBerry.html), gerne auch
mal [genug Bier für alle](https://flipdot.org/blog/archives/353-Heute-back-ich,-morgen-brau-ich,-und-uebermorgen-....html),
[programmieren](https://flipdot.org/blog/archives/246-flipdots-im-flipdot.html),
[alte Hardware retten](https://flipdot.org/blog/archives/253-thereifixedit.html),
[reparieren](https://flipdot.org/blog/archives/315-AVR-Programmer-repariert.html),
[hacken](https://flipdot.org/blog/archives/317-Bieg-den-Schaltkreis!.html),
[basteln](https://flipdot.org/blog/archives/335-Borg-Tetris.html),
[programmieren](https://flipdot.org/blog/archives/272-Dashboard-mit-Spacedaten.html),
[Vorträge](https://flipdot.org/blog/archives/133-Einfuehrung-in-die-Leiterplattenherstellung.html)
und
[Workshops](https://flipdot.org/blog/archives/311-Docker-Swarm-Workshop-heute-ab-1400-Uhr.html)
halten (auch mit und über
[Pizzabacken](https://flipdot.org/blog/archives/356-Pizzaprogrammiernacht-25.-27.-November.html)),
[tauschen Zeug](https://flipdot.org/blog/archives/268-Jaaa!-Weihnachtlicher-Sachentausch!.html),
[Baustelwochenenden](https://flipdot.org/blog/archives/277-Baustelwochenende.html)
und
[Pizzaworkshops](https://flipdot.org/blog/archives/107-Sonntag-Zum-digitalen-Datum-in-den-Hackerspace.html)
machen, zusammen über unsere Projekte, Anschaffungen, Pläne,
[Netzpolitik](https://flipdot.org/blog/archives/331-Die-Gentrifizierung-der-Hacker-und-Maker-Szene.html)
reden, unsere
[Räume](https://flipdot.org/blog/archives/161-Status-Update-Boden-legen.html)
herrichten,
[Besuch](https://flipdot.org/blog/archives/234-Space-Besucher-kommt-mit-Plan.html)
bekommen,
[andere Hackerspaces besuchen](https://flipdot.org/blog/archives/118-Visiting-metalab.html)
[2](https://flipdot.org/blog/archives/45-Besuch-im-SubLab-in-Leipzig.html),
[3](https://flipdot.org/blog/archives/11-Besuch-beim-Hackcenter-Treffen-in-The-Hub-Islington,-London.html),
[4](https://flipdot.org/blog/archives/337-Besuch-im-Open-Lab-in-Augsburg.html), auf
Konferenzen gehen oder einfach zusammen
[wegfahren](https://flipdot.org/blog/archives/117-flipdot-in-Wien.html).

## Wer seid ihr?

Unsere Mitglieder sind Informatiker, Schreiner, Schüler, Rentner, IT-Spezialisten, Kaminkehrer,
Physiker, Maschinenbauer, Lehrer, Azubis, und im Alter von 14 bis 64. So ist flipdot eine Ansammlung
von Know-How, die uns selber immer wieder staunen lässt. Viele von uns sind Mitglied im CCC oder dem
Club verbunden. flipdot ist auch der ERFA-Kreis Kassel, also die örtliche Gruppe des CCCs. Wir sind
als gemeinnütziger Verein organisiert. Der Verein ist eine Rahmenkonstruktion, damit wir z.B. Räume
mieten können, und ohne Bedeutung im Alltag vom Hackspace - wir treffen unsere Entscheidungen
kollektiv im Plenum.

## Wo seid ihr?

Im [Kulturbahnhof Kassel in der Nachristenmeisterei, Franz-Ulrich-Straße 18, 34117 Kassel](/kontakt/).
In der Nachrichtenmeisterei sind unter anderem auch ein Tonstudio, Bands, eine Taschenproduzentin,
ein Filmemacher und Werbeagenturen zu Hause, und die Kunsthochschule hat dort Räume, die regelmäßig
bespielt werden.

## Wann hat der Hackspace offen?

Unser Space ist jeden Dienstag ab 19:00 Uhr für Gäste geöffnet, aufgrund von COVID-19 aktuell aber
nur mit Impf- oder Genesenen-Nachweis (2G-Regelung). Stand: 2021-11-07

Mitglieder haben 24/7 Zugang und nutzen diesen auch.

## Muss ich Mitglied sein, um in den Space kommen zu dürfen?

Nein, der Dienstag ist offen für alle. Möchtest du regelmäßig und an allen anderen Tagen kommen
können, die Infrastruktur wie Server, Werkstätten, Küche nutzen und selbst einen Schlüssel haben,
dann musst du Mitglied werden.

## Muss ich Mitglied sein, um Veranstaltungen oder Workshops besuchen zu können?

Nein. Unsere offenen Veranstaltungen sind: Offen.

## Wie finanziert ihr euch?

Wir tragen uns schon fast ganz durch die Beiträge unserer Mitglieder. Ein befristeter Mietzuschuss
des CCC hat uns im Frühjahr 2016 ermöglicht, in die aktuellen Räume umzuziehen. Jedes Mitglied hilft
flipdot weiter und sicherer zu finanzieren.

## Und was kostet das für mich als Mitglied?

Bei der Gründung war unser Beitrag 36 € monatlich, womit wir genug Geld für den Aufbau gesammelt
haben. Inzwischen haben wir den Beitrag auf 23 € im Monat gesenkt. Wenn du dir das nicht leisten
kannst, zahlst du auf Antrag nur 10 €.

## Wie erreiche ich euch und wie bekomme ich mit, was bei euch passiert?

Was grad so läuft, liest du in unserem [Blog](/blog/). Da kannst du auch oben rechts sehen, ob der
Space gerade geöffnet oder geschlossen ist. Für Mitglieder führen wir ein geschlossenes Forum. Auf
[Twitter](https://twitter.com/flipdot_kassel) geben wir alle neue Blogeinträge und die nächsten
öffentlichen Treffen bekannt. Außerem erreichst du uns immer per mail: `com[ät]flipdot.org`. Alle
weiteren Kontaktdaten findest du [hier](/kontakt/).

## Woher kommt der Name "flipdot"?

Das ist ein kleines Plättchen mit einer hellen und einer dunklen Seite, ein „mechanisches Bit“.
Es wird zum Beispiel in den Anzeigen von Bussen verwendet und kann durch einen Stromimpuls
elektromechanisch gewendet werden.
