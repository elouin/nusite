+++
title = "Noch eine Spende"
date = 2010-06-30T13:07:21Z
author = "typ_o"
path = "/blog/2010/06/30/noch-eine-spende"
aliases = ["/blog/archives/97-Noch-eine-Spende.html"]
+++
[Conrad Elektronik](https://www.conrad.de/ce/) hat ein Päckchen
geschickt:

[![](/media/conrad03.serendipityThumb.jpg)](/media/conrad03.jpg)[![](/media/conrad04.serendipityThumb.jpg)](/media/conrad04.jpg)[![](/media/conrad02.serendipityThumb.jpg)](/media/conrad02.jpg)[![](/media/conrad01.serendipityThumb.jpg)](/media/conrad01.jpg)

Beim nächsten Bausteln gibts jede Menge Experimente - und ich bin
sicher, dass sich der eine oder andere Experimentierkasten sehr gut für
Modifikationen eignet!
