+++
title = "Wie man einen Blackout verursacht"
date = 2016-01-05T14:46:42Z
author = "typ_o"
path = "/blog/2016/01/05/wie-man-einen-blackout-verursacht"
aliases = ["/blog/archives/325-Wie-man-einen-Blackout-verursacht.html"]
+++
Sehr schöner Talk von 32C3: "[Wie man einen Blackout verursacht - und
warum das gar nicht so einfach
ist](https://media.ccc.de/v/32c3-7323-wie_man_einen_blackout_verursacht)".
Viele Details zum Aufbau der Netze und der Funktion der Netzregelung,
sowie deren Störungsempfindlichkeiten.

Wenn man sowieso schon [den ganzen Keller
voll](https://diyprepping.com/survivalist-prepper-homesteader/) Kerzen,
Ravioli und Bier hat, um der Zombieapokalypse widerstehen zu können:
Thomas Petermann et.Al.: "[Was bei einem Blackout geschieht - Folgen
eines langandauernden und großräumigen
Stromausfalls](https://www.tab-beim-bundestag.de/de/pdf/publikationen/buecher/petermann-etal-2011-141.pdf)"  
Veröffentlicht von: Studien des Büros für Technikfolgen-Abschätzung beim
Deutschen Bundestag.

Hier ist akribisch bis in psychologische Abschätzungen ausgebreitet, was
bei einem längeren Blackout passieren dürfte. Das ist die Studie, die in
dem Video erwähnt wurde.

[![](/media/IMG_1814.serendipityThumb.jpg)](/media/IMG_1814.jpg)

Das Bild ist von [american preppers
network](https://americanpreppersnetwork.com/). Dort: "Is Your Bunker
Ready For The Apocalypse? Starting construction on your family’s
emergency bunker could be one of the best decisions that you ever make".
