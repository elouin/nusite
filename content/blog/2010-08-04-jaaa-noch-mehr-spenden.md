+++
title = "Jaaa, noch mehr Spenden!"
date = 2010-08-04T05:11:31Z
author = "typ_o"
path = "/blog/2010/08/04/jaaa-noch-mehr-spenden"
aliases = ["/blog/archives/100-Jaaa,-noch-mehr-Spenden!.html"]
+++
Die Firma [Watterott Elektronic](https://www.watterott.com/), manchen als
Arduino und Adafruit Industries Distri bekannt, hat gespendet - und zwar
initiativ, wir wurden angesprochen, ob wir einen 19" Serverschrank,
Sicherungsautomaten, RCD-Schalter und diverse Evaluation-Boards brauchen
könnten. Ob wir das brauchen können? Hat der Papst einen komischen Hut?
Na klar können wir! Die beiden Deputy Chief Information Officer sind
sofort nach Hausen gefahren und haben die Ladung abgeholt. Besonders
gefällt uns das ARM-EVA-Board und das STK500 (schon nicht mehr alle auf
dem Foto, da sofort bespielt), quasi der Standard für Atmel-Entwicklung.
Vielen Dank an den Spender!  
[![](/media/watterott02.serendipityThumb.jpg)](/media/watterott02.jpg)[![](/media/watterott03.serendipityThumb.jpg)](/media/watterott03.jpg)[![](/media/watterott01.serendipityThumb.jpg)](/media/watterott01.jpg)
