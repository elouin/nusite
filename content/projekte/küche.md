+++
title = "Küche"
+++

Unsere Küche teilt sich in eine \'Tee\'küche und eine Spülküche.

## Hacker

- Dennis (Projektmanagement)
- BananaJoe (Smutje, Wasser-Anschluss)
- Wolfi (Wasser-Anschluss)

## Teeküche

Die Teeküche soll eine richtige Kochküche werden mit Abluft und
Kochplatten. Sie befindet sich im Flur und ist auch zugänglich für
Nachbarn und Gäste.

### Geräte/Schränke

- Kühlschrank (geliehen von Nachrichtenmeisterei)
- Campingkochplatten (geliehen von Dennis)
- Wasserkocher
- Holzschieber für Pfanne (selbst gemacht mit Invaderlogo)
- Pfanne (gusseisern repariert)
- Töpfe (geliehen von Helmut)
- Messer (BananaJoe)

### Nahrung

- Tee
- Salz und Pfeffer
- Zucker
- Fettbalken
- Kartoffeln
- Zwiebeln
- Möhren

### Fehlendes

- Dunstabzugshaube und [Abluftanlage](/projekte/abluftanlage/)
- Oberschränke für Gewürze und Essen
- Schloss für Kühlschrank
- Spendenbox für Essen
- Richtig gutes Küchenmesser (vielleicht bringt der Nachbar eines ein)
- Guter Salzstreuer
- Pfeffermühle
- ~~(Klobürste zum Kartoffelschälen)~~ normaler Kartoffelschäler

## Spülküche

Sie befindet sich im Bad und ist auch zugänglich für Nachbarn und Gäste.

### Geräte/Schränke

- Spülmaschine
- Spülbecken mit Warmwasseranschluss
- Unterschrank mit Putzzeug
- Spüllmittel
- Schwämme und Stahlwolle

### Fehlendes

- Systematisch Besteck und Geschirr für ca. 30 Leute
- Oberschränke für Geschirr
- Schubladensortiersystem für Besteck
