+++
title = "Hilfe vom Fricklerkommando"
date = 2014-05-07T19:01:47Z
author = "typ_o"
path = "/blog/2014/05/07/hilfe-vom-fricklerkommando"
aliases = ["/blog/archives/240-Hilfe-vom-Fricklerkommando.html"]
+++
Ein Gast brachte Teile eines Automaten mit in den Space, so ein
[Snackverkaufsdings mit
Drahtspiralen](https://image.channeladvisor.de/128021/8874d09f54c8f9aca8eba758928aed09.jpg).
Die Platinen mit den Motoren für die Spiralen und dem Endschalter, der
eine vollständige Spiralenumdrehung detektiert, sind nur zweipolig
angeschlossen. Wie geht aber die Auswertung des Schalters, wenn man nur
zwei Pole hat?

[![](/media/automat02.serendipityThumb.jpeg)](/media/automat02.jpeg)

[![](/media/automat01.serendipityThumb.jpeg)](/media/automat01.jpeg)

Eine
[Anfrage](https://www.fingers-welt.de/phpBB/viewtopic.php?f=14&t=1881) im
prima Forum von Fingers elektrischer Welt (Ja, das sind
[DIE](https://flipdot.org/blog/archives/40-Frickel-Punk.html)!)
brachte einen absolut treffsicheren Hinweis auf ein [US
Patent](https://www.google.com/patents/US4458187), mit dem die Stellung
des Schalters durch einen dem Motorstrom aufgeprägten Wechselstrom
[erkannt werden
kann](https://patentimages.storage.googleapis.com/pages/US4458187-6.png).
