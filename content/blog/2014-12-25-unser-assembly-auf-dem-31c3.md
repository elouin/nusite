+++
title = "Unser Assembly auf dem 31C3"
date = 2014-12-25T22:50:16Z
author = "typ_o"
path = "/blog/2014/12/25/unser-assembly-auf-dem-31c3"
aliases = ["/blog/archives/269-Unser-Assembly-auf-dem-31C3.html"]
+++
Wir haben uns einen ruhigen Platz gewünscht, und sind jetzt [im 1.
OG](https://events.ccc.de/congress/2014/wiki/images/7/78/31c3-map-v0_4_fullsize.png)
gelandet, zusammen mit allerlei wunderlichen anderen Gruppen, auch den
Bernds von Krautchan.
