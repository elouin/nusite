+++
title = "\"Sie\" beobachen mich."
date = 2013-04-30T10:05:00Z
author = "typ_o"
path = "/blog/2013/04/30/sie-beobachen-mich"
aliases = ["/blog/archives/202-Sie-beobachen-mich..html"]
+++
Dass ich paranoid bin, heisst nicht, dass sie *nicht* hinter mir her
sind. Details zu deiner ganz perönlichen Verfolgungstheorie im
[Conspiracy
Flowchart](https://1.bp.blogspot.com/-NUv7tx-DOTQ/UXKqqtaZ7CI/AAAAAAAAEoA/07uueHMiSJ4/s1600/Crispian's+Conspiracy+Flowchart.png).
Extrem zu empfehlen in diesem Kontext übrigens der Film: [Das weisse
Rauschen](https://de.wikipedia.org/wiki/Das_weisse_Rauschen). Ach -
wolltest du nicht schon längst deine Platte mit
[truecrypt](https://www.truecrypt.org/) verschlüsseln?

[![](/media/security.png)](https://xkcd.com/538/)

Ganz in unserer Nähe war übrigens grade die
[cryptocon](https://sublab.org/cryptocon13), im sublab in Leipzig.
(Extrem nette Leute da!).
