+++
title = "We make things awesome - we make awesome things. Baustelwochenende!"
date = 2010-10-20T05:32:01Z
author = "typ_o"
path = "/blog/2010/10/20/we-make-things-awesome-we-make-awesome-things-baustelwochenende"
aliases = ["/blog/archives/113-We-make-things-awesome-we-make-awesome-things.-Baustelwochenende!.html"]
+++
[![](/media/0230.serendipityThumb.jpg)](/media/0230.jpg)[![](/media/0255.serendipityThumb.jpg)](/media/0255.jpg)[![](/media/0020.serendipityThumb.jpg)](/media/0020.jpg)flipdot
- der hackerspace kassel - veranstaltet ein **Baustelwochenende vom
22.-24.10.**

Draußen wird es kalt und ungemütlich, drinnen verbreitet das Oszilloskop
und die Kontroll-LED vom Evaluation-Board ein gemütliches Licht, das
Flußmittel bewirkt weihnachtliche Gerüche, und der Nerd-made Pizzateig
geht auf.

Bring dein Projekt mit, löte, schraube, code und hab Spaß am Gerät.
[Hier die Einladung
(pdf)](https://flipdot.org/wiki/images/6/64/2010_10_22_Bausteln2.pdf).
Nähere Infos zum Projekt findet man im
[Wiki](https://web.archive.org/web/20120105031721/https://flipdot.org/wiki/index.php?title=Hallo!) und hier im Blog.
Wir werden u.A: an embedded Webservern arbeiten, das Projekt Blinkenglow
weitertreiben, die Raumbeleuchtung fernsteuern, Pizza backen, Club Mate
trinken und uns unterhalten. Worüber? Über Technik!  
Schon mal gucken? [Slideshow
hier!](/media/slideshows/offtuer2010-06/)
