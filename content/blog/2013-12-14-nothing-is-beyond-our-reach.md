+++
title = "Nothing is beyond our reach"
date = 2013-12-14T12:49:28Z
author = "typ_o"
path = "/blog/2013/12/14/nothing-is-beyond-our-reach"
aliases = ["/blog/archives/224-Nothing-is-beyond-our-reach.html"]
+++
Das [National Reconnaissance
Office](https://de.wikipedia.org/wiki/National_Reconnaissance_Office)
(NRO; deutsch Nationales Aufklärungsamt) ist ein 1960/61 gegründeter
Militärnachrichtendienst der USA, der für das militärische
Satellitenprogramm verantwortlich ist. Das NRO startete am 6. Dezember
2013 einen neuen Radar - Spiaonagesatelliten, NROL-39. Das [Logo des
Satelliten](https://i2.wp.com/vigilantcitizen.com/wp-content/uploads/2013/12/bawnccdceaeekoi.jpg)
mit dem Spruch "Nothing is Beyond our Reach" (links), verglichen mit
einer antikommunistischen Illustration mit dem Untertitel "Know Your
Communist Enemy" aus dem Werk von Robert B. Watts (1977), "Our Freedom
Documents", The Supreme Council, Washington.

[![](/media/nrol-39-mission-patch.serendipityThumb.jpg)](/media/nrol-39-mission-patch.jpg)[![](/media/tumblr_lu0gq2iYy21qaxtrf1.serendipityThumb.jpg)](/media/tumblr_lu0gq2iYy21qaxtrf1.jpg)

Die anderen Mission patches sind auch nicht ohne, please be sure to wear
your tinfoil hat!

[![](/media/Lacrosse4_L_patch.serendipityThumb.jpg)](/media/Lacrosse4_L_patch.jpg)[![](/media/NRO_L11_missionpatch.serendipityThumb.jpg)](/media/NRO_L11_missionpatch.jpg)

[![](/media/NROL32_patch.serendipityThumb.jpg)](/media/NROL32_patch.jpg)[![](/media/NROL-32_Patch.serendipityThumb.png)](/media/NROL-32_Patch.png)
