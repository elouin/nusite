+++
title = "Will it bend? [1]"
date = 2017-05-31T09:38:36Z
author = "typ_o"
path = "/blog/2017/05/31/will-it-bend-1"
aliases = ["/blog/archives/380-Will-it-bend-1.html"]
+++
Kleiner Test mit kunststoffbeschichtetem Multiplex-Plattenmaterial
(Quelle: Ein netter Casebauer ([so
was](https://www.musik-produktiv.de/pic-010053087_01l/dap-audio-rigging-flightcase_01l.jpg)),
der uns ab und zu seine Reste durchwühlen läßt)  

[![](/media/bend2.serendipityThumb.jpg)](/media/bend2.jpg)

Rot: Faserrichtung der äußersten Lage unter dem Kunststoff

Beim Gitarrenbau ist die Faserrichtung wie hier rot eingezeichnet,
deswegen habe ich das auch mal so versucht. Schlitze mit 3 mm Fräser, 3
mm Material dazwischen stehen gelassen, und so tief in Z-Richtung
eingetaucht, dass gerade noch die Außenlage unangetastet blieb.

Der Widerstand beim Biegen war merklich, auch kräftige Federwirkung. Die
Beschichtung ist in diesem Fall äußerst vorteilhaft und verhindert
splittern. Der Biegeradius war 43 mm.

\[1\] Natürlich
[dies](https://www.youtube.com/results?search_query=will+it+blend) ;)
