+++
title = "Round-Robin-Swap wg. Jahresendzeitfest"
date = 2014-12-19T04:25:13Z
author = "typ_o"
path = "/blog/2014/12/19/round-robin-swap-wg-jahresendzeitfest"
aliases = ["/blog/archives/267-Round-Robin-Swap-wg.-Jahresendzeitfest.html"]
+++
![](/media/reuse.gif)

Nächsten Dienstag, 23.12.: Jeder bringt Bebastelbares mit, was er zu
viel hat / mehrfacht hat / was schon lange rumliegt aber doch kein
Projekt wird, Bauteile, Gerät(chen), als Gehäuse geeignetes, Dickstrom,
Dünnstrom. Das tauschen wir dann kreuz und quer!
