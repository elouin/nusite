+++
title = "Arduino"
+++

Unter-Seiten:

* [Puls und Blut-Sauerstoff](puls-und-blut-sauerstoff/)

Alles um mit dem Mikrocontroller Arduino Projekte zu erstellen

- Arduino-IDE, die Software (Programm auf dem Computer um die Befehle
  zu übertragen)
  - <https://www.arduino.cc/en/Main/Software>
- Arduino mit dem Raspberry Pi programmieren (Einstellungen,
  Installationsguide, Hinweise)
  - Aktuelle Version für Raspian, ARM 32 runterladen, entpacken,
    install.sh ausführen (https://www.arduino.cc/en/Main/Software
    (??? Home Ordner)
  - Wie installieren <https://www.arduino.cc/en/guide/linux>
  - Probleme mit billigen Arduino Nano beim übertragen des
    Programms? -\> Einstellung "Old Bootloder" bei Arduino Boards
    <https://www.arduino.cc/en/Guide/ArduinoNano>
  - ? Ubuntu Mint auf Raspberry3 ???
    <https://ubuntu-mate.org/raspberry-pi/> \_! Experimental Version
    nehmen bei momentaner 18.04.2 beta, da bug, umd startet nicht bei recommended
    <https://ubuntu-mate.org/raspberry-pi/ubuntu-mate-18.04.2-beta1-desktop-arm64+raspi3-ext4.img.xz>
  - ? Ubuntu Server auf Raspberry 4 ???
    <https://ubuntu.com/download/raspberry-pi>
- Raspberry Workstations als PC, für Arduino, um in der Gruppe zu programmieren und zu basteln
  - 4x Raspi mit Monitor Maus und Tastatur, Arduino Kabel, LAN-Kabel und Hub ???
  - 1x Raspi um von ihm per LAN zu laden, alle das gleiche Image ???
- Projekte und Projektbeschreibungen / Anleitungen
