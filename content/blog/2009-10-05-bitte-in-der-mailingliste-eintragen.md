+++
title = "Bitte in der Mailingliste eintragen"
date = 2009-10-05T16:52:38Z
author = "typ_o"
path = "/blog/2009/10/05/bitte-in-der-mailingliste-eintragen"
aliases = ["/blog/archives/28-Bitte-in-der-Mailingliste-eintragen.html"]
+++
Die interne Mailingliste von flipdot (Low traffic) ist eingerichtet,
bitte trag dich <a href="#"><del>hier</del></a> ein.

(Du kriegst eine Mail, in der eine Zeile der Form **auth 2824a7f7
subscribe discuss-flipdot-org \[deine_mailadresse\]** zu finden ist.

Antworte auf diese mail, und setze diese Zeile als Nachrichtentext ein.
Fertig.

Die Adresse der Liste ist **discuss-flipdot-org {kringeldings}
flipdot.org**

Alle Mails haben im Betreff **\[discuss-flipdot-org\] Betrefftext**,
können also mit Filtern im Mailer bearbeitet werden.
