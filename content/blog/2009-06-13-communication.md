+++
title = "Communication"
date = 2009-06-13T07:10:00Z
author = "typ_o"
path = "/blog/2009/06/13/communication"
aliases = ["/blog/archives/13-Communication.html"]
+++
[**Blog**]  
</blog/> (du liest es gerade)

**Wiki**  
~~/wiki/~~

**Mail:**  
com {kringeldings} flipdot {punkt} org

**Mailingliste:**  
Mail an com {kringeldings} flipdot {punkt} org (Das macht dann ein
Mensch)

**Twitter**  
[FlipDot\_KS](https://twitter.com/flipdot_kassel)

**IRC**  
~~irc://irc.freenode.net/flipdot~~, auch als ~~Webchat~~ im Browser
