+++
title = "flipdot zieht um"
date = 2011-06-12T13:56:33Z
author = "typ_o"
path = "/blog/2011/06/12/flipdot-zieht-um"
aliases = ["/blog/archives/138-flipdot-zieht-um.html"]
+++
[![](/media/Bazille.serendipityThumb.jpg)](/media/Bazille.jpg)Zum
1.7. verlieren wir unsere Räume an eine Tangoschule. Naja, sowas muss es
wohl auch geben ... Die wollen für fünf Jahre mieten und umfangreich
renovieren, deshalb hat unsere Vermieterin denen zugesagt. Dennoch war
sie an unserer Anwesenheit im Haus interessiert, und hat uns neue Räume
angeboten: Im Keller gab es lange Jahre ein Autonomes Zentrum, die
"[Bazille](https://www.myspace.com/bazillekassel)". Der Keller, auf den
ersten Blick nicht für alle von uns reizvoll, hat dennoch was. Wir sind
gerade im Gespräch mit der Verwaltung und bekommen die nächsten Tage ein
Angebot für die renovierten Räume, jetzt auch mit Fenster zum
rausgucken. Am 23.6. packen wir unsere Sachen aus den aktuellen Räumen
in ein Zwischenlager, für den Übergang haben wir von einer anderen
Mieterin im Haus einen der hellgrünen Räume im Plan angeboten bekommen -
da können wir während des Umbaus sein...

Zum einen schade, weil wir viel Arbeit investiert haben, zum anderen gut
- wir werden Geld sparen, bleiben auf dem Gelände und bekommen eine
amtliche hackerspace - location unter der Erde ;)  

> **Edit:** wir haben uns gegen den Raum entschieden, weil das letzte
> Angebot dann doch gut 400 EUR monatliche Kosten bedeutet hätte - und
> das war uns schlicht zu viel!
