+++
title = "Geile Kiste"
date = 2013-06-26T08:42:39Z
author = "typ_o"
path = "/blog/2013/06/26/geile-kiste"
aliases = ["/blog/archives/210-Geile-Kiste.html"]
+++
Die Watterotts (das sind nette!) kamen gestern persönlich vorbei, und
haben uns mit einer
[Wanderkiste](https://www.watterott.com/de/blog/Wanderkiste) voller
Bastelmaterial beglückt. Bauteile, Krimskrams, Platinen - alles
hackable. Vielen Dank für die nette Aktion!

![](/media/kiste.jpg)

(Mir persönlich gefiel ja die Kiste selber am allerbesten ;)
