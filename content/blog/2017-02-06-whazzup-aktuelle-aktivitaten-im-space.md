+++
title = "Whazzup? Aktuelle Aktivitäten im Space"
date = 2017-02-06T06:54:33Z
author = "typ_o"
path = "/blog/2017/02/06/whazzup-aktuelle-aktivitaten-im-space"
aliases = ["/blog/archives/365-Whazzup-Aktuelle-Aktivitaeten-im-Space.html"]
+++
Die Küche ist jetzt in einem gut benutzbaren Zustand, wenn auch
Arbeitsplatz knapp ist, und die Spülen noch nicht optimal sind.

"Raum 4", der nach dem Einzug als Lagerraum mißbraucht wurde, ist jetzt
als Vortragsraum nutzbar und hält in einer Regalwand VIELE Euroboxen mit
Material und persönliche Projektboxen der Member. Leinwand und Beamer
sind da, mehr Strom und Netzwerk kommen noch.

[![](/media/2eb64ff099e00683e22f9ec68bbfcc98db613ae8_1_666x500.serendipityThumb.JPG)](/media/2eb64ff099e00683e22f9ec68bbfcc98db613ae8_1_666x500.JPG)

In der Lounge kam vor einigen Wochen ein großer Arbeitstisch dazu
(massives Untergestell vom Sperrmüll, Buchenplatte aus dem
Baumarkt).

Hier einige Member-Projekte im Anriß, später gibt es Details.

**\[nein\] Datenanalyse (Vergleich) verschiedener Zeitungen.** (Passend dazu:
[SpiegelMining -- Reverse Engineering von
Spiegel-Online](https://media.ccc.de/v/33c3-7912-spiegelmining_reverse_engineering_von_spiegel-online))

**\[dargmuesli\] Nachbau der unterbrechungsfreien Stromversorgung für den Raspberry Pi1.**
([dieser](https://flipdot.org/blog/archives/348-USV-fuer-raspberry-Pi-11.-Juni-2016,-2000.html))
TODO:
[Löten lernen](https://www.b-redemann.de/download/loeten.pdf)!

[![](/media/usv-supply.serendipityThumb.jpg)](/media/usv-supply.jpg)

**\[Wolfi\] Aufbau einer Totmannschaltung für Herd und Pizzaofen.** (Selbstabschaltung nach n
Minuten bei Inaktivität).

**\[DMB\] Bau eines Mikrowellen-Transformator-Schweißgerätes.**

**\[cfstras\] Getränkeverwaltung**
([Github](https://github.com/flipdot/drinks-scanner-display))

- Registrierte Member können jetzt einen beliebigen Barcode als ID-card
  verwenden, oder einen Barcode mit Nicknamen ausdrucken
- Mit ID-card ist Getränk buchen in 5 Sekunden möglich: Getränk scannen,
  ID-card scannen, "Trinken" drücken!
- WIP: Anonyme barcodes, die man sich für 5 Euro ausdrucken kann, und dann
  leertrinken!

![](/media/b1cf15bd5e62d125645f4197a92062aef5368308_1_666x500.serendipityThumb.jpg)

**\[Dino, DMB, Nils\] Aufbau 3D Drucker.**

[![](/media/fc2b1e7856367c6c7a17a61beb50891d73f5fb7d_1_666x500.serendipityThumb.jpg)](/media/fc2b1e7856367c6c7a17a61beb50891d73f5fb7d_1_666x500.jpg)

**\[Freifunkmann\] DMX Funk Sticks im Space testen (Reichweite), OpenWRT, OpenVPN einrichten.**

**\[Typ_o\] Test von batteriebetriebenen ESP8266 - Sensoren.** Ziel: Erfassung der Zustände von
Oberlichten, Fenstern, Türen, Kühlschrank etc. im Space - also Dinge,
die man beim Verlassen des Space vergessen kann.

[![](/media/df964a48adb4002107add3cde170a4724a8d5507.serendipityThumb.jpg)](/media/df964a48adb4002107add3cde170a4724a8d5507.jpg)

**\[Doktor\] Frontblende für zwei [DPS5015](https://www.aliexpress.com/item/d/32776423798.html) China DC-DC-Platinen**
(Aluminium, Herstellung auf der CNC Fräse)

[![](/media/82e3fceff8ae7097cbe97922b748ef5a32f121b5_1_690x359.serendipityThumb.png)](/media/82e3fceff8ae7097cbe97922b748ef5a32f121b5_1_690x359.png)
