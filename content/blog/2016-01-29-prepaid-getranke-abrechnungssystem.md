+++
title = "Prepaid Getränke-Abrechnungssystem"
date = 2016-01-29T16:17:02Z
author = "typ_o"
path = "/blog/2016/01/29/prepaid-getranke-abrechnungssystem"
aliases = ["/blog/archives/329-Prepaid-Getraenke-Abrechnungssystem.html"]
+++
Pseudo- / Anonym, mit paper trail, extrem niedriger
Standby-Stromverbrauch, niedrige Systemkosten, geringer Schulungsaufwand
für die User, einfache Administrierbarkeit.

![](/media/prepaidblatt.jpg)

[prepaid.pdf](/media/prepaid.pdf "prepaid.pdf"),
/
[prepaid.ods](/media/prepaid.ods "prepaid.ods")
/
[prepaid-user-training.pdf](/media/prepaid-user-training.pdf "prepaid-user-training.pdf")
