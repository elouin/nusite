+++
title = "Muss.Fräsen!"
date = 2015-04-18T11:52:35Z
author = "typ_o"
path = "/blog/2015/04/18/muss-frasen"
aliases = ["/blog/archives/275-Muss.Fraesen!.html"]
+++
[![](/media/20150417_132626.serendipityThumb.jpg)](/media/20150417_132626.jpg)

Unsere Fräse ist da! Verfahrweg, also bearbeitbare Fläche, ca 500 x 600
mm. Durch die offene Konstruktion kann man auch breitere und sehr viel
längere Werkstücke einlegen!

Jetzt sind noch ein paar Kleinigkeiten zu tun: Licht aufhängen & Strom
im Werkstattraum legen, Motorsteuerung installieren, GRBL Interpreter
bauen, Stromversorgung für die beiden Komponenten dazu, alles schön in
ein Gehäuse & verkabeln, unter den Tisch montieren, Schublade für den
Laptop, Notaus-Kette bauen, GRBL auf die Maschinenphysik einstellen.
Dann CAM Software für den Steuerrechner auswählen, und dann üben, üben,
üben...!

Hier noch eine wilde Linksammlung zu unserem CNC Thema, die bei der
Maschinenauswahl entstanden ist:

## CNC

- [Guerrilla CNC home manufacturing
  guide](https://lcamtuf.blogspot.de/2010/07/guerrilla-cnc-home-manufacturing-guide.html)
- Peters CNC Ecke - [Forum](https://www.cncecke.de/forum/forum.php)
- [GRBL G Code Interpreter on
  Arduino](https://dank.bengler.no/-/page/show/5470_grbl?ref=checkpoint) der / die Macher
- [Connecting Grbl](https://github.com/grbl/grbl/wiki/Connecting-Grbl)
- [Github](https://github.com/grbl/grbl)
- [Cofiguring GRBL 0.9](https://github.com/grbl/grbl/wiki/Configuring-Grbl-v0.9) Github
- Laserplotter mit
  [GRBL](https://blog.domestichacks.info/2013/05/diy-laserplotter-cnc-teil-6-software/)
- GRBL [setup Tutorial](https://www.youtube.com/watch?v=1ioctbN9JV8)
- [CNC Workflow](https://www.youtube.com/watch?v=107FGoYX1bg): Sketchup,
  Makercam, GRBL Controller
- GRBL [CNC Tutorial](https://www.youtube.com/watch?v=sGzLnUrcYYY) unter Verwendung
  von [engineeringforless](https://engineeringforless.com/efl_cnc.html) (grbl
  uploader zum uC und com.programm zur Benutzung)
- GRBL
  [Wiki](https://letsgoingwiki.reutlingen-university.de/mediawiki/index.php/Wissen:_grbl)
- [Disable homing](https://github.com/grbl/grbl/issues/224) Z axis
- [Universal G Code
  Sender](https://github.com/winder/Universal-G-Code-Sender) (Java)
- [GRBL Controller](https://zapmaker.org/projects/grbl-controller-3-0/)
- [GRBLWeb](https://xyzbots.com/grblweb.html) Gcode sender, RasPi Images
- [chilipeppr](https://chilipeppr.com/) - browser based g code sender
- G code sender
  [1](https://www.shapeoko.com/wiki/index.php/Grbl_Controller),
  [SerialComCNC](https://www.mikrocontroller.net/topic/345380)
- Jog Wheel, Fachbegriff: Pendant
  - [Color
    Code](https://www.google.de/imgres?imgurl=http%3A%2F%2Fi01.i.aliimg.com%2Fimg%2Fpb%2F204%2F826%2F466%2F466826204_050.jpg&imgrefurl=http%3A%2F%2Fwww.shapeoko.com%2Fforum%2Fviewtopic.php%3Ff%3D7%26t%3D1454&h=400&w=400&tbnid=ZqfZoKc0sq6rnM%3A&zoom=1&docid=ah5SFnIqtZ3-vM&ei=eyf9VLLIIcSsPePygIgF&tbm=isch&iact=rc&uact=3&dur=1654&page=1&start=0&ndsp=21&ved=0CC0QrQMwBA) XYZ
  - [Physical](https://www.cnccookbook.com/img/LatheStuff/CNCConversion/CNCPanels/NemiconPendant.jpg) User
    Interface
  - [more](https://github.com/grbl/grbl/issues/243)
- [Tips for routing
  Aluminium](https://blog.cnccookbook.com/2012/03/27/10-tips-for-cnc-router-aluminum-cutting-success/)
- estlcam
- [CamBam](https://www.cambam.info/) dxf 2 grbl
- librecad
- [Blog](https://www.precifast.de/)
- Elektromaschinen:  
  Margarethe Wiegand An- und Verkauf,  
  [Mühlenweg 25 a, 34246
  Vellmar](https://maps.google.de/maps?q=+M%C3%BChlenweg+25+a,+34246+Vellmar&hl=de&ie=UTF8&ll=51.354846,9.476051&spn=0.027899,0.066047&sll=51.314886,9.460615&sspn=0.223388,0.528374&hnear=M%C3%BChlenweg+25A,+Niedervellmar+34246+Vellmar&t=m&z=15),  
  Tel.: 20 25 741, 0177 7421868, Herr Walter Wiegand
- LinuxCNC (wird im DIY Lab benutzt)
- LinuxCNC auf
  dem [RasPi](https://www.raspberrypi.org/forums/viewtopic.php?f=37&t=33809)
- Fräser
  - [VHM
    Fräser](https://www.as-toolstore.de/epages/62215969.sf/de_DE/?ObjectPath=/Shops/62215969/Products/2312/SubProducts/%222312%200040%200200%2003175%203800%22) 1
    Schneider
