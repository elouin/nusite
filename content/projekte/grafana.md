+++
title = "Grafana"
+++

Mit [Grafana](https://stats.flipdot.org/) können wir die gemessenen Daten des Spaces darstellen
und analysieren. Vgl. [Neuland/Cyber](Neuland/Cyber)

## Daten

- Eingeloggte member im WLAN (security by obscurity)
- Eingeloggte Nutzer im flipdot Freifunk
- Öffnungszeiten des Space
- Internet-Traffic im Space (Up/Down)
- [Stromverbrauch](/projekte/watts-up-pi/)
- Bewegungsmelder (pro Raum)
- Toiletten Besetzt/Freistatus
- Toilettenpapierstand
- [Getränkefüllstand](/projekte/getränkezähler/) (pro Getränkesorte)
- CO2 Werte
- Lautstärke (pro Raum)
- Öffnungszeiten im Space (vorerst Flipdot Display Ping später Türstatus)
- Türsensoren unten
- Temperatur im Space (über die Heizung)
- [Abluftanlage](/projekte/abluftanlage/)
