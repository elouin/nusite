+++
title = "flipdot @ CCCKS"
date = 2011-08-23T06:50:41Z
author = "typ_o"
path = "/blog/2011/08/23/flipdot-cccks"
aliases = ["/blog/archives/143-flipdot-CCCKS.html"]
+++
~~Wir treffen uns heute und bis auf weiteres jeden Dienstag um 19:00~~
[in den Räumen des CCC
Kassel](https://maps.google.de/maps?q=kassel,+wilhelmsh%C3%B6her+allee+73&hl=de&ie=UTF8&ll=51.311308,9.47338&spn=0.006284,0.011061&sll=51.312273,9.473723&sspn=0.006284,0.011061&t=h&z=17):

In der Ingenieur-Schule Kassel - Via Bahnanbindung einfach mit der 1
oder 3 Richtung Bahnhof Willhelmshöhe aus der Innenstadt. Haltestelle
Murhardstraße. Unser Raum befindet sich im Keller, Nr. -1307. (Tipp:
Eingang B, rechts direkt runter bis in den Keller, dann rechts in den
Gang).

Wenn du Probleme hast, den Raum zu finden: Mail an com {kringeldings}
flipdot {punkt} org

Edit: Diese Meldung ist nicht mehr aktuell, da <a href="#"><del>neuer Raum</del></a> gefunden!
