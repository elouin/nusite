+++
title = "OSM-Stammtisch Nordhessen"
date = 2019-05-08T01:15:52Z
author = "Baustel"
path = "/blog/2019/05/08/osm-stammtisch-nordhessen"
aliases = ["/blog/archives/429-OSM-Stammtisch-Nordhessen.html"]
+++
![](/media/osm-kassel.jpg)

Am **Dienstag, den 21. Mai 2019**, findet um **18:30** der erste
OpenStreetMap-Stammtisch Nordhessen / Südniedersachsen statt.

Ziel des OSM-Stammtisches soll sein, dass sich Anwender der Region über
die gemeinsame Arbeit im Projekt austauschen und zukünftige planen
können.

Die Veranstaltung findet in den Räumen des Nordhessischen
VerkehrsVerbundes im Hauptbahnhof statt.

Mehr Informationen und kostenfreie Anmeldung unter
<https://wiki.openstreetmap.org/wiki/Stammtisch_Nordhessen>.
