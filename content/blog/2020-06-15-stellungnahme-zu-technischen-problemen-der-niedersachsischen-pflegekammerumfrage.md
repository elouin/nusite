+++
title = "Stellungnahme zu technischen Problemen der Niedersächsischen Pflegekammerumfrage"
date = 2020-06-15T19:56:24Z
author = "Baustel"
path = "/blog/2020/06/15/stellungnahme-zu-technischen-problemen-der-niedersachsischen-pflegekammerumfrage"
aliases = ["/blog/archives/443-Stellungnahme-zu-technischen-Problemen-der-Niedersaechsischen-Pflegekammerumfrage.html"]
+++
Vor kurzem erhielten wir Hinweise auf mögliche technische Mängel in der
*Pflegekammerumfrage Niedersachsen*. Ca. 80.000 Pflegekräfte sollten in
dieser Umfrage Stellung zu ihrer Einstellung zur Pflegekammer und deren
Arbeit nehmen. In dem Fragebogen wurden neben den fachlichen Fragen
grobe Angaben zum Tätigkeitsbereich, Berufserfahrung, Alter und
Postleitzahlbereichen erhoben.

Wir möchten anhand der uns vorliegenden Informationen einige Dinge
einordnen.

Ein Link zur Umfrage wurde öffentlich geteilt. Augenscheinlich war es
möglich, die Umfrage ohne weitere Authentifizierung durchzuführen. Durch
den Link ließen sich die Antworten einer bereits teilweise ausgefüllten
Umfrage einsehen. Beim Neuladen der Seite wurden unterschiedliche
Antworten angezeigt. Die Ursache können wir als Außenstehende nicht
zweifelsfrei ermitteln und es Bedarf einer Aufklärung durch den
Dienstleister.

> Besonders ärgere die Ministerin, dass durch die unerlaubten Zugriffe
> Pflegekräfte wertvoller Zeit beraubt wurden: „Die Pflegekräfte sind in
> ihrem Beruf außergewöhnlichen Belastungen ausgesetzt und uns ist sehr
> bewusst, dass die Beteiligung an einer umfangreichen Umfrage eine
> zusätzliche zeitliche Belastung darstellt. Durch die
> Manipulationsversuche wurde dieser Zeitaufwand nun mutwillig zunichte
> gemacht.“
>
> — [Dr. Carola
> Reiman](https://www.ms.niedersachsen.de/startseite/service_kontakt/presseinformationen/verdacht-auf-unerlaubte-zugriffe-189093.html)

Uns liegen keine Hinweise vor, dass ein mutwilliger Angriff erfolgte.
Das Umfragesystem hat ein ungewöhnliches Verhalten gezeigt, welches
Anlass zu Bedenken gegeben hat. Diese Bedenken wurden an den Betreiber
herangetragen, welcher entsprechend reagiert hat. Durch den öffentlichen
Link besteht eventuell die Möglichkeit, dass jedermann versehentlich
Antworten geändert haben könnte.

Wir hoffen auf eine technische Aufarbeitung des zugrundeliegenden
Problems durch den Dienstleister.

Wir bedanken uns an der Stelle bei den Beteiligten für ihr Vertrauen und
den verantwortungsvollen Umgang mit dem Vorfall. Der Betreiber der
Umfrage hat schnell reagiert, um einen möglichen Missbrauch zu
vermeiden.

## Quellen und weiterführende Informationen:

- <https://www.bffk.de/aktuelles/pflegekammerumfrage-in-niedersachsen-gestoppt-vollstaendiger-neustart-wahrscheinlich.html>
- <https://www.ms.niedersachsen.de/startseite/service_kontakt/presseinformationen/verdacht-auf-unerlaubte-zugriffe-189093.html>
- [NDR: Hacker Alarm Befragung zu Pflegekammer gestoppt
  (YouTube)](https://www.youtube.com/watch?v=UmOgqo3kDv8)
