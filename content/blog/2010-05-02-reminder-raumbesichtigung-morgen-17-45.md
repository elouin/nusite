+++
title = "Reminder: Raumbesichtigung morgen 17:45"
date = 2010-05-02T13:49:59Z
author = "typ_o"
path = "/blog/2010/05/02/reminder-raumbesichtigung-morgen-17-45"
aliases = ["/blog/archives/86-Reminder-Raumbesichtigung-morgen-1745.html"]
+++
Am Montag, den 3. Mai um 17:45 Uhr treffen wir uns kurz vorher im [Döner
Imbiss](https://maps.google.de/maps?q=51.318338,9.49247&num=1&sll=51.318928,9.49601&sspn=0.111645,0.256119&ie=UTF8&ll=51.318337,9.491973&spn=0.001227,0.005493&z=18)
links neben der Lolita Bar um uns zu koordinieren, und gehen dann zu dem
Besichtigungstermin in die Sickingenstraße 10, 34117 Kassel um mit den
Vermietern die
[Räume](https://flipdot.org/wiki/index.php?title=Raumsuche/Sickingenstrasse)
anzusehen.  
Kommt alle, damit wir eine breite Entscheidungsbasis haben.
