+++
title = "Geplanter Staatstrojaner in Hessen gefährdet IT-Sicherheit weltweit"
date = 2017-11-09T09:16:50Z
author = "typ_o"
path = "/blog/2017/11/09/geplanter-staatstrojaner-in-hessen-gefahrdet-it-sicherheit-weltweit"
aliases = ["/blog/archives/394-Geplanter-Staatstrojaner-in-Hessen-gefaehrdet-IT-Sicherheit-weltweit.html"]
+++
Im Bundesland Hessen soll ein Gesetz die geheimdienstliche Ausweitung
der Nutzung von  
Staatstrojanern erlauben. Wir fordern: Kein Staatstrojaner für Hessen!

Der von den Fraktionen der CDU und Bündnis90/Die Grünen im Hessischen
Landtag vorgelegte „[Gesetzesentwurf zur Neuausrichtung des
Verfassungsschutzes in
Hessen](https://www.gruene-hessen.de/landtag/files/2017/10/HSVG.pdf)“
sieht eine Ausweitung der Befugnisse des Verfassungsschutzes vor,
insbesondere den Einsatz von Staatstrojanern zur sog.
Quellen-Telekommunikationsüberwachung („Quellen-TKÜ“) und zur
Online-Durchsuchung. Mit der Informationsseite
[www.hessentrojaner.de](https://hessentrojaner.de) möchten die
hessischen Chaos Computer Clubs über die Funktionsweise und Gefahren von
Staatstrojanern informieren.

<!-- more -->

„Eine solche Regelung gefährdet die Sicherheit Millionen vernetzter
Geräte weltweit. Der Einsatz von Staatstrojaner setzt verwundbare
Software in Smartphones oder Laptops voraus“, erklärt Markus Drenger vom
CCC Darmstadt. „Verantwortungsbewusste Sicherheitspolitik zielt darauf
ab, dass Sicherheitslücken schnellstmöglich geschlossen werden, damit
diese nicht von Kriminellen ausgenutzt werden können. Mit einem
Staatstrojaner hat der Staat jedoch ein Interesse daran, offene
Hintertüren nicht zu schließen, um diese später selbst nutzen zu
können. Wissen über Lücken vor Herstellern geheimzuhalten, um
Sicherheitsupdates zu verhindern, schadet der IT-Sicherheit.“

Sicherheitslücken, wie sie für Staatstrojaner und andere Schadsoftware
notwendig sind, werden aufgrund ihrer enormen Tragweite für teilweise
sechs- bis siebenstellige Eurobeträge gehandelt. Da solche Lücken oft in
weit verbreiteten Anwendungen klaffen, stellen sie ein enormes
Gefährdungspotenzial für eine große Zahl von Geräten dar. „Hiervon sind
auch kritische Infrastrukturen wie beispielsweise Krankenhäuser,
Windparks oder Atomkraftwerke betroffen“, betont Magnus Frühling vom CCC
Frankfurt. „Die Vergangenheit hat leider gezeigt, dass sogar finanziell
und personell gut ausgestattete Behörden wie die NSA nicht in der Lage
sind, die Geheimhaltung dieser Lücken sicherzustellen.“

Im Mai diesen Jahres erregte ein [von Kriminellen verbreiteter
Erpressungs-Trojaner](https://faktenfinder.tagesschau.de/wanna-cry-cyberangriff-101.html)
namens „Wannacry“ weltweit Aufsehen, da er neben Privat-PCs auch
Automobilkonzerne, Bahnunternehmen und Krankenhäuser lahmlegte. Der
geschätzte finanzielle Schaden betrug bis zu [vier Milliarden
Euro](https://www.handelsblatt.com/finanzen/geldpolitik/globale-cyber-attacke-so-viel-verdienten-die-wannacry-erpresser/19830290.html).
Die von Wannacry genutzte Lücke in Microsoft Windows war der NSA bereits
seit Jahren bekannt. „Anstatt die Sicherheitsmängel dem Hersteller
Microsoft zu melden und so ein Sicherheitsupdate zu ermöglichen, hat der
Geheimdienst diese aber zur Nutzung durch Staatstrojaner
geheimgehalten“, schildert Marco Holz vom CCC Darmstadt das Problem.
„Außerdem können auch repressive Regime im Ausland die von
Steuergeldern in Deutschland finanzierten Hacking-Tools zum Ausspähen
von Journalisten, Oppositionspolitikern und unterdrückten Minderheiten
nutzen. Der Zweitverwertungsmarkt für Sicherheitslücken und Trojaner ist
groß. Die Technologie-Zulieferer solcher Regierungen [sitzen oft in
Europa](https://www.zeit.de/digital/datenschutz/2017-02/ueberwachung-technik-exporte-europa-kontrolle-versagt).“  
Für Unternehmen stellen offene Sicherheitslücken auch [unter dem Aspekt
der
Wirtschaftsspionage](https://www.hessen.de/pressearchiv/pressemitteilung/sicherheitsluecken-schaden-betrieben-0)
eine Gefahr dar. Vor den Gefahren durch Sicherheitslücken für deren
IT-Sicherheit warnte auch das hessische Wirtschaftsministerium bei der
Vorstellung des [hessischen
IT-Sicherheitsleitfadens](https://wirtschaft.hessen.de/sites/default/files/media/hmwvl/leitfaden_vertraulichkeitsschutz_durch_verschluesselung.pdf),
der gemeinsam mit dem Fraunhofer-Institut für Sichere
Informationstechnologie (SIT) in Darmstadt erarbeitet wurde.

Wir fordern die Abgeordneten im Hessischen Landtag vor diesem
Hintergrund auf, dem Gesetzentwurf in der derzeitigen Form nicht
zuzustimmen.

flipdot hackerspace kassel, ERFA Kassel des Chaos Computer Clubs
