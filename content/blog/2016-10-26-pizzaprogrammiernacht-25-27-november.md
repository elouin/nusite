+++
title = "Pizzaprogrammiernacht 25. - 27. November"
date = 2016-10-26T14:35:33Z
author = "typ_o"
path = "/blog/2016/10/26/pizzaprogrammiernacht-25-27-november"
aliases = ["/blog/archives/356-Pizzaprogrammiernacht-25.-27.-November.html"]
+++
Wir hacken nicht nur, wir backen auch. In unserem neuen
Gastro-Pizzaofen: Große Mengen Pizza, bis jeder satt ist. Währenddessen
kannst du coden, löten, fräsen, Git und Github kennen lernen, dich mit
Kotlin und Golang beschäftigen, und dich im Hackerspace umschauen. Start
am Freitag um ca 20:00, Ende am Sonntag.  
Unkostenbeitrag für das Essen sind 5 EUR pro Person.

Aktuelle Infos: telnet ppn.flipdot.org 2323

**Anmeldung nötig: com //aet// flipdot.org**

[Flyer
(pdf)](/media/PPN-2016-11-25.pdf "PPN-2016-11-25.pdf")

[![](/media/ppn-flyer-vorn.serendipityThumb.jpg)](/media/ppn-flyer-vorn.jpg)
