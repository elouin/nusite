+++
title = "flipdot auf dem Tag der offenen Tür der Uni Kassel"
date = 2017-04-20T22:55:00Z
author = "typ_o"
path = "/blog/2017/04/20/flipdot-auf-dem-tag-der-offenen-tur-der-uni-kassel"
aliases = ["/blog/archives/375-flipdot-auf-dem-Tag-der-offenen-Tuer-der-Uni-Kassel.html"]
+++
![](/media/tdot.serendipityThumb.jpg)

Gut besucht, viele Interessenten und viele interessante Gespräche.
Hinter uns der Stand der Amateurfunker: Wie aus einer anderen Zeit, cqcq
gemorst, aber so recht hat keiner geantwortet. Eigentlich schade.
