+++
title = "Was Sie schon immer über Technik wissen wollten..."
date = 2017-05-14T11:10:04Z
author = "typ_o"
path = "/blog/2017/05/14/was-sie-schon-immer-uber-technik-wissen-wollten"
aliases = ["/blog/archives/377-Was-Sie-schon-immer-ueber-Technik-wissen-wollten....html"]
+++
…aber nie zu fragen wagten!

Das Thema dieses Tages ist Programm, und trotz allen Spaßes den uns das
Vorbeiten und Durchführen bereitet, meinen wir das vollkommen Ernst.

Wenn Du gerne einfach mal einen etwas tieferen Einblick in die Welt der
Technik, der Software, der Elektronik, des Programmierens oder des
Konstruierens haben möchtest, aber bisher einfach kaum Erfahrungen hast,
dann gönn‘ dir den Spaß und komm vorbei!

Wir öffnen an diesem Tag unseren
[Hackerspace](/faq/) für offene Workshops. Wir
überlegen welche Themen Dich interessieren, in welchen Bereichen Du
gerne mehr erfahren möchtest oder es häufiger bei Dir klemmt und Du nur
noch Fragezeichen siehst. Trau Dich [und schick uns doch einfach Deine
Themenvorschläge ein](/kontakt/), wir schauen dann
ob wir es an diesem Tag als Workshop anbieten können. Du willst Freunde
und Bekannten davon erzählen und zum mitkommen anregen? [Hier der
Flyer](https://flipdot.org/wiki/Was%20Sie%20schon%20immer%20%C3%BCber%20Technik%20wissen%20wollten...?action=AttachFile&do=view&target=FD_technikworkshop.pdf)

Mitmachen, Fragen, Interesse und Neugier sind nicht nur erlaubt sondern
ausdrücklich erwünscht!

Ausserdem zeigen wir an dem Tag unsere Projekte und Einrichtung, laden
Dich ein Fragen zu stellen und mitzumachen.

Das Programm [ist
hier](https://flipdot.org/wiki/Was%20Sie%20schon%20immer%20%25C3%25BCber%20Technik%20wissen%20wollten...)
(Runterscrollen, Änderungen bis zuletzt möglich)

Samstag 20.05.2017, Ende offen  
13:00 Uhr  
Franz-Ulrich-Straße 18 Kassel
