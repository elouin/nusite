+++
title = "Flipdot-Beiträge gesenkt"
date = 2012-08-14T14:49:00Z
author = "ruben"
path = "/blog/2012/08/14/flipdot-beitrage-gesenkt"
aliases = ["/blog/archives/171-Flipdot-Beitraege-gesenkt.html"]
+++
Schon vor dem Sommerloch beschlossen, nun werbewirksam auch im Blog
verkündet:

Der Minimalbeitrag für reguläre Mitglieder beträgt nun 23 €, der
reduzierte (Schüler, Studenten, ...) 10 € pro Monat. (Die meisten der
bisherigen Mitglieder lassen aber ihre alten Beiträge von 36 € weiter
laufen)
