+++
title = "Ein guter Zug: Wissenschaft und Forschung"
date = 2009-10-17T01:00:00Z
author = "typ_o"
path = "/blog/2009/10/17/ein-guter-zug-wissenschaft-und-forschung"
aliases = ["/blog/archives/34-Ein-guter-Zug-Wissenschaft-und-Forschung.html"]
+++
Ein Ausstellungszug zu Forschung und Entwicklung [für drei Tage in
Kassel](https://www.expedition-zukunft.org/alias/Kassel/985327) vom 2.
bis 4.11.09.
