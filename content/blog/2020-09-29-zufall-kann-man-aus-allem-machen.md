+++
title = "Zufall kann man aus allem machen"
date = 2020-09-29T16:22:02Z
author = "flipdot member"
path = "/blog/2020/09/29/zufall-kann-man-aus-allem-machen"
aliases = ["/blog/archives/445-Zufall-kann-man-aus-allem-machen.html"]
+++
Ein Vortrag über Zufall und mit dem Computer generierten Zufall - verständlich für Jeden!

[Zufall.pdf](/media/Zufall.pdf)
