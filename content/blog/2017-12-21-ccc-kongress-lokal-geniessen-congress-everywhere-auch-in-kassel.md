+++
title = "CCC Kongress lokal genießen / Congress everywhere auch in Kassel"
date = 2017-12-21T16:30:40Z
author = "typ_o"
path = "/blog/2017/12/21/ccc-kongress-lokal-geniessen-congress-everywhere-auch-in-kassel"
aliases = ["/blog/archives/395-CCC-Kongress-lokal-geniessen-Congress-everywhere-auch-in-Kassel.html"]
+++
Bereits zum 34ten mal öffnet der Chaos Computer Club zwischen den
Feiertagen die Pforten zum [Chaos Communication
Congress](https://de.wikipedia.org/wiki/Chaos_Communication_Congress).
Während den vier Tagen gibt es auf dem Leipziger Messegelände
zahlreiche Talks und Workshops zu verschiedensten Themen aus IT,
Security, Kultur, Bildung, Raumfahrt, uvm.. Eine Liste aller Vorträge
findet sich
[hier](https://events.ccc.de/congress/2017/Fahrplan/index.html).

Für alle die kein Ticket ergattern konnten oder wollten, öffnen wir vom
27. bis 30.12. jeweils ab ca. Mittag unseren Hackspace
([Anfahrt](/kontakt/)) und laden ein zum
gemeinsamen Streams schauen, bausteln und chillen bei jeder Menge
Blinkenlights. Voraussichtlich bauen wir unter Anderem auch an unserem
automatischen Getränke-Bestellsystem mit MQTT-vernetzten Waagen weiter,
und an unseren Fenster- und Türsensoren-Projekt. Zwischendurch kochen
wir und backen wir Pizza. Wir freuen uns auf die gemeinsame Gespräche
und Diskussionen rund um die Vorträge oder mitgebrachte Themen.

Außerdem ist das ein guter Zeitpunkt sich mal den Hackspace anzuschauen.
Elektro- und Mechanik-Werkstatträume, 3D-Drucker, CNC Fräse, Mikroskop,
allerlei Meßgeräte und viele andere Dinge können gemeinsam erforscht und
benutzt werden. Wenn wir dein Interesse wecken konnten, du aber dennoch
Fragen hast, melde dich einfach via
[Kontakt](/kontakt/).

[![](/media/Bildschirmfotovom2017-12-21174213.serendipityThumb.png)](/media/Bildschirmfotovom2017-12-21174213.png)

Die
[Fairydust](https://www.heise.de/newsticker/meldung/34C3-Der-Chaos-Communication-Congress-geht-nach-Leipzig-3713516.html),
das Maskotchen der CCC Veranstaltungen.
