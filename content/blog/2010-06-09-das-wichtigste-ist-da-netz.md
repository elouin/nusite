+++
title = "Das Wichtigste ist da: Netz"
date = 2010-06-09T04:20:35Z
author = "typ_o"
path = "/blog/2010/06/09/das-wichtigste-ist-da-netz"
aliases = ["/blog/archives/91-Das-Wichtigste-ist-da-Netz.html"]
+++
[![](/media/schalter.serendipityThumb.jpg)](/media/schalter.jpg)[![](/media/net_io.serendipityThumb.jpg)](/media/net_io.jpg)[![](/media/material.serendipityThumb.jpg)](/media/material.jpg)[![](/media/lan.serendipityThumb.jpg)](/media/lan.jpg)Interfaces
zu den Lichtschaltern, Interfaces zu den Interfaces, Baustel-Material,
das Netz

[![](/media/schild.serendipityThumb.jpg)](/media/schild.jpg)Gestern
haben wir den geborgten Router an das vorhandene Netz der Werkstätten
nebenan angeschlossen (Wlan geht sogar im Arkadas!), dreckige Möbel
geputzt, die Spülmaschine ist angeschlossen und spült, unser flipdot
Logo ist auf den orangenen Würfel am Eingang geklebt (Klebefolie von
Pfannkuch & Laserdrucker). Die Deckenlampen sind jetzt per Lan zu
betätigen (Test).

Wir haben jetzt 4 komplette Schlüsselsätze, arazer denkt über eine
"Space offen" Meldung auf der Website nach. (kann man bis da hin ja
manuell im Wiki bzw in Twitter machen)
