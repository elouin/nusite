+++
title = "Ein ganz normaler Dienstag"
date = 2018-04-04T06:01:11Z
author = "typ_o"
path = "/blog/2018/04/04/ein-ganz-normaler-dienstag"
aliases = ["/blog/archives/401-Ein-ganz-normaler-Dienstag.html"]
+++
Beim Rundgang durch den Space sah man gestern: Den Bau eines Gehäuses
für einen RasPi-Radiowecker aus einem Stapel Multiplex-Platten, die
Konstruktion eines zweistufigen (Aktivkohle)filters für den 3D-Druck mit
feuerfestem (UL 94 V-0) Filament, einen Versuchsaufbau für ein
Galvanometer aus einem Strohhalm und einem mit Gold besputterten
Glasplättchen und erfolgreiche Tests zur Beeinflussung des
Gleichgewichtsorgans durch einen Gleichstrom vom Linken zum rechten Ohr.
