+++
title = "Durchsuchungen in Hackspaces"
date = 2018-07-05T21:01:17Z
author = "typ_o"
path = "/blog/2018/07/05/durchsuchungen-in-hackspaces"
aliases = ["/blog/archives/409-Durchsuchungen-in-Hackspaces.html"]
+++
Die Polizei in Bayern hat kürzlich Wohnungen und Geschäftsräume von
Mitgliedern, insbesondere den Vorständen des Vereins Zwiebelfreunde
durchsucht. Dabei waren sie noch nicht einmal selbst einer Straftat
verdächtig, sondern lediglich Zeugen. Der Grund: Ein Blog, welcher sich
offen gegen Positionen der AfD stellte, nutzte die E-Mail-Services eines
alternativen Mail-Providers [RiseUp](https://riseup.net/), für den die
Zwiebelfreunde aktiv geworben und spenden gesammelt haben.

<!-- more -->

Möglich machte [diese
Durchsuchung](https://netzpolitik.org/2018/zwiebelfreunde-durchsuchungen-wenn-zeugen-wie-straftaeter-behandelt-werden/)
offenbar das [Polizeiaufgabengesetz in
Bayern](https://www.br.de/nachrichten/das-neue-polizeiaufgabengesetz-kurz-erklaert-100.html),
welches im Frühsommer diesen Jahres entgegen aller Warnungen von
Bürgerrechtsgruppen, Verfassungsrechtlern und sogar aus Polizeikreisen
vom Landtag in München verabschiedet wurde. Ähnliche Ereignisse stehen
uns auch in NRW bevor, sollte der Landtag in Düsseldorf die geplanten
Gesetze abnicken, die unter dem Begriff der „drohenden Gefahr“
erhebliche Grundrechtseingriffe zur Folge haben werden. [Deswegen
demonstriert mit uns am 7.7. in Düsseldorf gegen diese neuen
Gesetze.](https://www.no-polizeigesetz-nrw.de/demo-7-7/)

Am Abend des 4. Juli rückte die Polizei, genauer: die Zentral- und
Anlaufstelle Cybercrime NRW in den „Langen August“ in Dortmund ein und
durchsuchte neben dem alternativen Hoster [FREE!](https://www.free.de/)
unter anderem die Räume unserer Freunde beim Chaostreff Dortmund, aber
auch die der anderen Vereine und Initiativen. Offenbar ging es den
Beamten darum, Dokumente sicherzustellen, die als geheim eingestuft
waren, aber bereits veröffentlicht worden sind sowie diejenigen Personen
zu finden, die die [Dokumente einst
geleakt](https://www1.wdr.de/nachrichten/ruhrgebiet/cybercrime-razzia-dortmund-nordstadt-100.html)
haben. Hier ging es wohl auch darum, den Quellenschutz auf
technisch-taktische Weise zu unterlaufen.

Auch die Gesetzesänderungen im letzten Jahr trugen ihren Teil dazu bei.
Die offensichtlichsten Neueinführungen, wie beispielsweise die
Online-Durchsuchung, kaschierten relativ gut die Änderungen an der
Strafprozessordnung, nach der Zeugen Aussagen nicht mehr rundheraus
verweigern dürfen. Der renommierte Strafverteidiger Udo Vetter hat dazu
im vorigen Jahr schon [ein paar
Zeilen](https://www.lawblog.de/index.php/archives/2017/06/23/schoene-neue-zeugenwelt/)
geschrieben.

Lasst uns wachsam bleiben und aufeinander acht geben. Je mehr wir Hack-
und Makerspaces unseren sozialen Stellenwert in unseren Städten und
Quartieren herausstellen und verdeutlichen, desto besser sind wir in
Zukunft geschützt vor unwidersprochener Willkür.

([Quelle](https://www.devtal.de/blog/2018/07/05/pag-firstfallout/))

**Reporter ohne Grenzen** schreibt:

„Das Vorgehen der Strafverfolgungsbehörden ist absolut unverhältnismäßig
und auch als Angriff gegen Anonymität im Internet anzusehen“, sagte
ROG-Geschäftsführer Christian Mihr. „Das Vorgehen schüchtert
Netzaktivisten, aber auch Journalisten ein, die sich für sichere
Kommunikation im Internet einsetzen. Die Behörden müssen alle
beschlagnahmten Geräte und Dokumente sofort zurückgeben und die
Hintergründe ihres Vorgehens erklären.“
([Quelle](https://www.reporter-ohne-grenzen.de/deutschland/alle-meldungen/meldung/solidaritaet-mit-netzaktivisten/))

Weitere Quellen zu den Durchsuchungen:

[CCC](https://www.ccc.de/de/updates/2018/hausdurchsuchungen-bei-vereinsvorstanden-der-zwiebelfreunde-und-im-openlab-augsburg)  
[Spiegel
Online](https://www.spiegel.de/netzwelt/web/hausdurchsuchungen-bei-netzaktivisten-chaos-computer-club-kritisiert-polizeivorgehen-a-1216463.html)  
[Heise
Online](https://www.heise.de/newsticker/meldung/Massive-CCC-Kritik-an-Polizei-Hausdurchsuchung-bei-Datenschutz-Aktivisten-4098764.html)  
[Netzpolitik.org](https://netzpolitik.org/2018/zwiebelfreunde-durchsuchungen-wenn-zeugen-wie-straftaeter-behandelt-werden/)  
[Augsburger
Allgemeine](https://www.augsburger-allgemeine.de/augsburg/Als-der-Staatsschutz-ins-Augsburger-Open-Lab-kam-id51549741.html?utm_campaign=Echobox&utm_medium=augsburg&utm_source=Twitter#Echobox=1530695341)  
[Radio
Dreyeckland](https://rdl.de/beitrag/beschlagnahme-bei-riseup-tor-und-tails-support-netzwerk-und-ccc-nahen-openlab-augsburg)
