+++
title = "flipdot auf dem 30C3"
date = 2013-12-26T20:53:37Z
author = "typ_o"
path = "/blog/2013/12/26/flipdot-auf-dem-30c3"
aliases = ["/blog/archives/228-flipdot-auf-dem-30C3.html"]
+++
fuer alle die zum 30c3 kommen und nicht so lange die Flipdot assembly  
suchen wollen. Wir haben einen Tisch im Hackcenter (Erdgeschoss)
hinten  
links. (via mail von fd member)  
EDIT: [Beta 0.2 der
Karte](https://events.ccc.de/congress/2013/wiki/images/0/06/30c3-map_v0.2-beta2.png)
der assemblies. Lokale Kopie (Flipdot: ganz links unten!):

[![](/media/30c3-map_v0.2-beta2.serendipityThumb.png)](/media/30c3-map_v0.2-beta2.png)
