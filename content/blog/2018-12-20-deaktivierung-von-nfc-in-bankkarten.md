+++
title = "Deaktivierung von NFC in Bankkarten"
date = 2018-12-20T23:08:05Z
author = "Baustel"
path = "/blog/2018/12/20/deaktivierung-von-nfc-in-bankkarten"
aliases = ["/blog/archives/424-Deaktivierung-von-NFC-in-Bankkarten.html"]
+++
Wir haben über die Lokalzeitung HNA die allgemeine Bevölkerung darauf
hingewiesen, dass man die NFC-Funktion ausschalten kann, wenn man deren
Verwendung nicht wünscht.

![](/media/2018-12-11-hna.serendipityThumb.png)

[Auf den Seiten des CCC Aachen findet sich eine
Tabelle](https://calc.ccc.ac/Girokarten_NFC_deaktivieren), in der Nutzer
ihre Erfahrungen mit dem Abschalten von NFC in Bankkarten sammeln. Wenn
du Informationen dazu beitragen kannst würde uns das sehr freuen.
