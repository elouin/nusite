+++
title = "15 Oszillatoren in fünf Minuten"
date = 2016-01-05T18:44:30Z
author = "typ_o"
path = "/blog/2016/01/05/15-oszillatoren-in-funf-minuten"
aliases = ["/blog/archives/326-15-Oszillatoren-in-fuenf-Minuten.html"]
+++
Eine Hand voll Blink-LED

Viel interessanter ist das Geräusch beim Abschalten:

Die Geräusche erinnern sehr stark an
[Spherics](https://de.wikipedia.org/wiki/Sferics), die Freaks, die in
dem Bereich lauschen, nennen sie auch
[Whistler](https://www.astrosurf.com/luxorion/audiofiles-geomagnetosphere.htm).
