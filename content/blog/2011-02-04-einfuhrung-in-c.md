+++
title = "Einführung in C"
date = 2011-02-04T19:28:04Z
author = "typ_o"
path = "/blog/2011/02/04/einfuhrung-in-c"
aliases = ["/blog/archives/128-Einfuehrung-in-C.html"]
+++
Am 8.2.2011 ca. 20:00 gibt es den nächsten Vortrag im flipdot
hackerspace kassel: Einführung in die Programmiersprache C (von Ric0).  
Dies ist ein Einstiegskurs, in dem die Grundlagen angesprochen werden,
mit Beispielen vom "Hello World" bis zum Taschenrechner.
