+++
title = "Sendung im Freien Radio Kassel"
date = 2016-11-12T23:12:49Z
author = "typ_o"
path = "/blog/2016/11/12/sendung-im-freien-radio-kassel"
aliases = ["/blog/archives/357-Sendung-im-Freien-Radio-Kassel.html"]
+++
Flipdot war heute im
[FRK](https://www.freies-radio-kassel.de/startseite.html) zu Gast. Hier
wie in der Sendung versprochen die erwähnten Links. Zum
[Download](/media/2016-11-12-Flipdot-Freies-Radio-Kassel.mp3)
das Interview als Ausschnitt der Sendung.

- [Chaosradio Express 134](https://cre.fm/cre134-hackerspaces) Hackerspaces
- Womit beschäftigt sich der CCC? [Chaosradio](https://chaosradio.ccc.de/)
  zeigt das sehr vollständig.
- Zu den [Themen von Flipdot](https://vimeo.com/99108416), Anfangszeit
- [Spass am Gerät](https://www.youtube.com/watch?v=0GU1SDkmk8w)
- Doku vom letzten [Baustelwochenende](https://vimeo.com/128079323)
- [Freifunk](https://freifunk.net//) bundesweit
- [Alternativlos](https://alternativlos.org/), der Podcast von Frank Rieger
  und fefe

Die erwähnten Hackerspaces:

- [Metalab](https://metalab.at/) in Wien
- [Sublab](https://sublab.org/) in Leipzig
- London [Hackspace](https://london.hackspace.org.uk/)
- [Noisebridge](https://www.noisebridge.net/) in San Franziskio
- [To Labaki](https://wiki.tolabaki.gr/w/To_LABaki) in Heraklion auf Kreta
